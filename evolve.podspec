Pod::Spec.new do |spec|
    spec.name                  = 'evolve'
    spec.version               = '0.1.0'
    spec.homepage              = 'https://gitlab.com/kmm-pokemon/evolve'
    spec.source                = { :git => 'https://gitlab.com/kmm-pokemon/evolve.git', :tag => spec.version.to_s }
    spec.license               = { :type => 'MIT', :file => 'LICENSE' }
    spec.summary               = 'Provide browse and evolve pokemon'
    spec.authors      		   =  { 'tossaro' => 'hamzah.tossaro@gmail.com' }
    spec.source_files 		   = "evolve_ios/evolve/Classes/**/*.{swift}"
    spec.resources             = "evolve_ios/evolve/Resources/**/*.{gif,png,jpeg,jpg,storyboard,xib,xcassets}"
    spec.libraries             = 'c++'
    spec.ios.deployment_target = '14.1'
    spec.static_framework      = true
    spec.dependency 'evolve_shared'
end
