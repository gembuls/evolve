package example.pokemon.evolve.shared.domain.evolve

import example.pokemon.evolve.shared.domain.evolve.entity.Berry
import example.pokemon.evolve.shared.domain.evolve.entity.Pokemon
import example.pokemon.evolve.shared.domain.evolve.entity.PokemonUrl

interface EvolveRepository {
    suspend fun getPokemonsNetwork(offset: Int, limit: Int): MutableList<PokemonUrl>
    suspend fun getPokemonsLocal(
        offset: Int,
        limit: Int,
        search: String,
        isChosen: Boolean?
    ): List<PokemonUrl>

    suspend fun setPokemonsLocal(pokemons: MutableList<PokemonUrl>)

    suspend fun getPokemonDetailNetwork(name: String): Pokemon?
    suspend fun getPokemonDetailLocal(name: String): Pokemon?
    suspend fun setPokemonDetailLocal(pokemon: Pokemon)

    suspend fun getBerriesNetwork(offset: Int, limit: Int): MutableList<Berry>
    suspend fun getBerriesLocal(): List<Berry>
}