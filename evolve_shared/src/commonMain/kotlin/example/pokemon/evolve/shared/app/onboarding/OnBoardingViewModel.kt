package example.pokemon.evolve.shared.app.onboarding

import example.pokemon.core.shared.app.common.BaseViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class OnBoardingViewModel : BaseViewModel() {
    val onNext = MutableStateFlow(false)
    val hasChosen = MutableStateFlow(false)

    fun next() {
        onNext.value = true
    }

    fun choose() {
        scope.launch {
            loadingIndicator.value = true
            delay(1000)
            loadingIndicator.value = false
            hasChosen.value = true
        }
    }
}