package example.pokemon.evolve.shared.data.evolve.network.response

import kotlinx.serialization.Serializable

@Serializable
data class PokemonItemResp(
    var id: Int? = null,
    var name: String? = null,
    var sprites: PokemonSpritesItem? = null,
)