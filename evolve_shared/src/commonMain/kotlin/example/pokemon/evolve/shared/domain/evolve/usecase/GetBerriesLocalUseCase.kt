package example.pokemon.evolve.shared.domain.evolve.usecase

import example.pokemon.evolve.shared.domain.evolve.EvolveRepository
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class GetBerriesLocalUseCase : KoinComponent {
    private val evolveRepository: EvolveRepository by inject()
    suspend operator fun invoke() = evolveRepository.getBerriesLocal()
}