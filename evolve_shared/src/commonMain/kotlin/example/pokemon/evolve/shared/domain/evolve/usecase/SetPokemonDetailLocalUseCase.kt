package example.pokemon.evolve.shared.domain.evolve.usecase

import example.pokemon.evolve.shared.domain.evolve.EvolveRepository
import example.pokemon.evolve.shared.domain.evolve.entity.Pokemon
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class SetPokemonDetailLocalUseCase : KoinComponent {
    private val evolveRepository: EvolveRepository by inject()
    suspend operator fun invoke(pokemon: Pokemon) = evolveRepository.setPokemonDetailLocal(pokemon)
}