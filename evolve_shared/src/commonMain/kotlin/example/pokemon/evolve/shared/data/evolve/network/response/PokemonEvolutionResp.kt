package example.pokemon.evolve.shared.data.evolve.network.response

import example.pokemon.evolve.shared.domain.evolve.entity.PokemonEvolution
import kotlinx.serialization.Serializable

@Serializable
data class PokemonEvolutionResp(
    var chain: PokemonEvolution? = null,
)