package example.pokemon.evolve.shared.domain.evolve.entity

import example.pokemon.evolve.shared.data.evolve.network.response.PokemonItemResp
import io.realm.kotlin.types.RealmObject
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
open class Berry : RealmObject {
    var id: Int? = null
    var name: String? = null
    var weight: Int? = null
    var image: String? = null
    var item: PokemonUrl? = null
    var itemDetail: PokemonItemResp? = null
    var firmness: PokemonUrl? = null

    @SerialName("natural_gift_type")
    var naturalGiftType: PokemonUrl? = null
}