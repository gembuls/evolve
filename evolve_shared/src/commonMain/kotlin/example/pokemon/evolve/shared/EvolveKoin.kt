package example.pokemon.evolve.shared

import example.pokemon.core.shared.Context
import example.pokemon.core.shared.initKoin
import example.pokemon.evolve.shared.domain.evolve.entity.*
import example.pokemon.evolve.shared.domain.evolve.usecase.*
import io.ktor.http.*
import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration

fun protocolShared() = URLProtocol.HTTPS
fun prefsNameShared() = "pri0r_m1cr0_h1ght_3v0lv3"
fun provideStockDb(): Realm {
    val config = RealmConfiguration.Builder(
        schema = setOf(
            Berry::class,
            PokemonUrl::class,
            Pokemon::class,
            PokemonStat::class,
            PokemonType::class,
            PokemonEvolution::class,
        )
    ).build()
    return Realm.open(config)
}


@Suppress("UnUsed")
fun initKoin(
    context: Context?,
    host: String,
    deviceId: String,
    version: String,
) = initKoin(context, host, protocolShared(), prefsNameShared(), deviceId, version) {
    modules(libModule())
}