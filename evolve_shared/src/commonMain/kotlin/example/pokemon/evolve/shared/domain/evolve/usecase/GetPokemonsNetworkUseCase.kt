package example.pokemon.evolve.shared.domain.evolve.usecase

import example.pokemon.core.shared.external.constant.AppConstant
import example.pokemon.evolve.shared.domain.evolve.EvolveRepository
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class GetPokemonsNetworkUseCase : KoinComponent {
    private val evolveRepository: EvolveRepository by inject()
    suspend operator fun invoke(offset: Int, limit: Int = AppConstant.LIST_LIMIT) =
        evolveRepository.getPokemonsNetwork(offset, limit)
}