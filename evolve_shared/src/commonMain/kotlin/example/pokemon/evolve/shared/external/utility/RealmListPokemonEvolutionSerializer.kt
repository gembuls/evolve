package example.pokemon.evolve.shared.external.utility

import example.pokemon.evolve.shared.domain.evolve.entity.PokemonEvolution
import io.realm.kotlin.ext.realmListOf
import io.realm.kotlin.types.RealmList
import kotlinx.serialization.KSerializer
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

class RealmListPokemonEvolutionSerializer(val dataSerializer: KSerializer<PokemonEvolution>) :
    KSerializer<RealmList<PokemonEvolution>> {
    override fun serialize(encoder: Encoder, value: RealmList<PokemonEvolution>) {
        encoder.encodeSerializableValue(ListSerializer(dataSerializer), value.toList())
    }

    override fun deserialize(decoder: Decoder): RealmList<PokemonEvolution> {
        val list = realmListOf<PokemonEvolution>()
        val items = decoder.decodeSerializableValue(ListSerializer(dataSerializer))
        list.addAll(items)
        return list
    }

    override val descriptor: SerialDescriptor = ListSerializer(dataSerializer).descriptor
}