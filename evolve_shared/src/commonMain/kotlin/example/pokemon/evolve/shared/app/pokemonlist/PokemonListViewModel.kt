package example.pokemon.evolve.shared.app.pokemonlist

import example.pokemon.core.shared.app.common.BaseViewModel
import example.pokemon.core.shared.external.constant.AppConstant
import example.pokemon.evolve.shared.domain.evolve.entity.PokemonUrl
import example.pokemon.evolve.shared.domain.evolve.usecase.GetPokemonsLocalUseCase
import example.pokemon.evolve.shared.domain.evolve.usecase.GetPokemonsNetworkUseCase
import example.pokemon.evolve.shared.domain.evolve.usecase.SetPokemonsLocalUseCase
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class PokemonListViewModel : KoinComponent, BaseViewModel() {
    private val getPokemonsNetworkUseCase: GetPokemonsNetworkUseCase by inject()
    private val getPokemonsLocalUseCase: GetPokemonsLocalUseCase by inject()
    private val setPokemonsLocalUseCase: SetPokemonsLocalUseCase by inject()

    var page = 0
    var search = ""
    var isChosen: Boolean? = null
    var allPokemons = MutableStateFlow<MutableList<PokemonUrl>>(mutableListOf())
    var pokemons = MutableStateFlow<MutableList<PokemonUrl>?>(null)
    var pokemon = MutableStateFlow<PokemonUrl?>(null)

    fun getPokemonsLocal(limit: Int = AppConstant.LIST_LIMIT, searchArg: String? = null) {
        scope.launch {
            loadingIndicator.value = true
            val pokemonsLocal = getPokemonsLocalUseCase(
                limit * page,
                limit,
                searchArg ?: search,
                isChosen
            )
            pokemonsLocal.takeIf { it.isNotEmpty() }?.let {
                if (searchArg != null) pokemon.value = it[0]
                else {
                    if (limit > AppConstant.LIST_LIMIT) {
                        allPokemons.value = it.toMutableList()
                        pokemons.value = it.take(AppConstant.LIST_LIMIT).toMutableList()
                    } else pokemons.value = it.toMutableList()
                }
                loadingIndicator.value = false
            } ?: run {
                if (isChosen == null) getPokemonsNetwork()
                else {
                    delay(350)
                    pokemons.value = mutableListOf()
                    loadingIndicator.value = false
                }
            }
        }
    }

    private fun getPokemonsNetwork() {
        scope.launch {
            loadingIndicator.value = true
            try {
                val response = getPokemonsNetworkUseCase(0, 100000)
                allPokemons.value = response
                loadingIndicator.value = false
                setPokemonsLocalUseCase(response)
                delay(1000)
                getPokemonsLocal()
            } catch (e: Exception) {
                e.printStackTrace()
                pokemons.value = mutableListOf()
                page = 0
                getPokemonsLocal()
            }
        }
    }
}