package example.pokemon.evolve.shared.domain.evolve.entity

import io.realm.kotlin.types.RealmObject
import kotlinx.serialization.Serializable

@Serializable
open class PokemonStat : RealmObject {
    var base_stat: Int? = null
    var stat: PokemonUrl? = null
}