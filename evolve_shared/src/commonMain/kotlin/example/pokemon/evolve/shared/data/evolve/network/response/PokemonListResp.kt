package example.pokemon.evolve.shared.data.evolve.network.response

import example.pokemon.evolve.shared.domain.evolve.entity.PokemonUrl
import kotlinx.serialization.Serializable

@Serializable
data class PokemonListResp(
    var results: MutableList<PokemonUrl>? = null
)