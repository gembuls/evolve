package example.pokemon.evolve.shared.external.utility

import example.pokemon.evolve.shared.domain.evolve.entity.PokemonStat
import io.realm.kotlin.ext.realmListOf
import io.realm.kotlin.types.RealmList
import kotlinx.serialization.KSerializer
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

class RealmListPokemonStatSerializer(val dataSerializer: KSerializer<PokemonStat>) :
    KSerializer<RealmList<PokemonStat>> {
    override fun serialize(encoder: Encoder, value: RealmList<PokemonStat>) {
        encoder.encodeSerializableValue(ListSerializer(dataSerializer), value.toList())
    }

    override fun deserialize(decoder: Decoder): RealmList<PokemonStat> {
        val list = realmListOf<PokemonStat>()
        val items = decoder.decodeSerializableValue(ListSerializer(dataSerializer))
        list.addAll(items)
        return list
    }

    override val descriptor: SerialDescriptor = ListSerializer(dataSerializer).descriptor
}