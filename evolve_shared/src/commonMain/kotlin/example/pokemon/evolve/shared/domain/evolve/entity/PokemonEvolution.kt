package example.pokemon.evolve.shared.domain.evolve.entity

import example.pokemon.evolve.shared.external.utility.RealmListPokemonEvolutionSerializer
import io.realm.kotlin.types.RealmList
import io.realm.kotlin.types.RealmObject
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
open class PokemonEvolution : RealmObject {
    @SerialName("evolves_to")
    @Serializable(with = RealmListPokemonEvolutionSerializer::class)
    var evolvesTo: RealmList<PokemonEvolution>? = null
    var species: PokemonUrl? = null
    var id: Int? = null
}