package example.pokemon.evolve.shared.data.evolve.network.response

import example.pokemon.evolve.shared.domain.evolve.entity.PokemonUrl
import kotlinx.serialization.Serializable

@Serializable
data class BerryListResp(
    var results: MutableList<PokemonUrl>? = null,
)