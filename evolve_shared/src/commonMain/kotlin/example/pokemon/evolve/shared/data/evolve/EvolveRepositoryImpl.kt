package example.pokemon.evolve.shared.data.evolve

import example.pokemon.core.shared.external.utility.ApiClient
import example.pokemon.evolve.shared.data.evolve.network.response.*
import example.pokemon.evolve.shared.domain.evolve.EvolveRepository
import example.pokemon.evolve.shared.domain.evolve.entity.Berry
import example.pokemon.evolve.shared.domain.evolve.entity.Pokemon
import example.pokemon.evolve.shared.domain.evolve.entity.PokemonUrl
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class EvolveRepositoryImpl : KoinComponent, EvolveRepository {
    private val apiClient: ApiClient by inject()
    private val realm: Realm by inject()

    override suspend fun getPokemonsNetwork(offset: Int, limit: Int): MutableList<PokemonUrl> {
        val resp: PokemonListResp? =
            apiClient.client.get("/api/v2/pokemon?limit=$limit&offset=$offset").body()
        val temp = mutableListOf<PokemonUrl>()
        resp?.results?.let {
            for ((i, item) in it.withIndex()) {
                item.order = i
                temp.add(item)
            }
        }
        return temp
    }

    override suspend fun getPokemonsLocal(
        offset: Int,
        limit: Int,
        search: String,
        isChosen: Boolean?
    ): List<PokemonUrl> {
        return when {
            isChosen != null && search.isNotEmpty() -> realm.query<PokemonUrl>(
                "order >= $0 AND isChosen = $1 AND name = $2",
                offset,
                isChosen,
                search
            ).limit(limit).find()
            isChosen != null && search.isEmpty() -> realm.query<PokemonUrl>(
                "order >= $0 AND isChosen = $1",
                offset,
                isChosen
            ).limit(limit).find()
            isChosen == null && search.isNotEmpty() -> realm.query<PokemonUrl>(
                "order >= $0 AND name = $1",
                offset,
                search
            ).limit(limit).find()
            else -> realm.query<PokemonUrl>("order >= $0", offset).limit(limit)
                .find()
        }
    }

    @Suppress("kotlin:S1192")
    override suspend fun setPokemonsLocal(pokemons: MutableList<PokemonUrl>) {
        for (pokemon in pokemons) {
            realm.write {
                val exist = query<PokemonUrl>("name = $0", pokemon.name).first().find()

                var temp = PokemonUrl()
                if (exist != null) temp = exist
                else temp.name = pokemon.name
                temp.order = pokemon.order
                temp.url = pokemon.url
                temp.isChosen = pokemon.isChosen
                if (temp.isChosen == null || temp.pokemon == null) temp.pokemon = pokemon.pokemon
                if (exist == null) copyToRealm(temp)
            }
        }
    }

    override suspend fun getPokemonDetailNetwork(name: String): Pokemon? {
        val pokemon: Pokemon? = apiClient.client.get("/api/v2/pokemon/$name").body()
        pokemon?.also { p ->
            p.species?.url?.let { u1 ->
                val species: PokemonSpeciesResp? = apiClient.client.get(u1).body()
                species?.let { s ->
                    s.evolutionChain?.url?.let { u2 ->
                        val evolution: PokemonEvolutionResp? = apiClient.client.get(u2).body()
                        p.evolution = evolution?.chain
                    }
                }
            }
            p.weightCache = p.weight
            p.image = p.sprites?.other?.officialArtwork?.frontDefault
        }
        return pokemon
    }

    @Suppress("kotlin:S1192")
    override suspend fun getPokemonDetailLocal(name: String): Pokemon? =
        realm.query<Pokemon>("name = $0", name).first().find()

    @Suppress("kotlin:S1192")
    override suspend fun setPokemonDetailLocal(pokemon: Pokemon) = realm.write {
        val exist = query<Pokemon>("name = $0", pokemon.name).first().find()

        var temp = Pokemon()
        if (exist != null) temp = exist
        else temp.name = pokemon.name
        temp.id = pokemon.id
        temp.weightCache = pokemon.weightCache
        temp.weight = pokemon.weight
        temp.image = pokemon.image
        if (temp.evolution == null) temp.evolution = pokemon.evolution
        if (temp.sprites == null) temp.sprites = pokemon.sprites
        if (temp.stats == null) temp.stats = pokemon.stats
        if (temp.types == null) temp.types = pokemon.types
        if (exist == null) copyToRealm(temp)
    }

    override suspend fun getBerriesNetwork(offset: Int, limit: Int): MutableList<Berry> {
        val resps: BerryListResp? =
            apiClient.client.get("/api/v2/berry?limit=$limit&offset=$offset").body()
        val berries = mutableListOf<Berry>()
        resps?.results?.let { its ->
            for (it in its) {
                val berry: Berry? = apiClient.client.get(it.url.toString()).body()
                berry?.let { b ->
                    b.weight = when (b.firmness?.name) {
                        "very-soft" -> 2
                        "soft" -> 3
                        "hard" -> 5
                        "very-hard" -> 8
                        "super-hard" -> 10
                        else -> 1
                    }
                    val item: PokemonItemResp? = apiClient.client.get(b.item?.url.toString()).body()
                    item?.let { i ->
                        b.itemDetail = i
                        b.image = i.sprites?.default
                        realm.write { copyToRealm(b) }
                        berries.add(b)
                    }
                }
            }
        }
        return berries
    }

    override suspend fun getBerriesLocal(): List<Berry> = realm.query<Berry>().find()
}