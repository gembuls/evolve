package example.pokemon.evolve.shared.data.evolve.network.response

import example.pokemon.evolve.shared.domain.evolve.entity.PokemonUrl
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PokemonSpeciesResp(
    @SerialName("evolution_chain")
    var evolutionChain: PokemonUrl? = null,
    var name: String? = null,
)