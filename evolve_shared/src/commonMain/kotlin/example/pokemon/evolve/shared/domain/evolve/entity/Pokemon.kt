package example.pokemon.evolve.shared.domain.evolve.entity

import example.pokemon.evolve.shared.data.evolve.network.response.PokemonSpritesResp
import example.pokemon.evolve.shared.external.utility.RealmListPokemonStatSerializer
import example.pokemon.evolve.shared.external.utility.RealmListPokemonTypeSerializer
import io.realm.kotlin.types.RealmList
import io.realm.kotlin.types.RealmObject
import kotlinx.serialization.Serializable

@Serializable
open class Pokemon : RealmObject {
    var id: Int? = null
    var name: String? = null
    var weight: Int? = null
    var weightCache: Int? = null
    var evolution: PokemonEvolution? = null

    @Serializable(with = RealmListPokemonStatSerializer::class)
    var stats: RealmList<PokemonStat>? = null

    @Serializable(with = RealmListPokemonTypeSerializer::class)
    var types: RealmList<PokemonType>? = null
    var sprites: PokemonSpritesResp? = null
    var species: PokemonUrl? = null
    var image: String? = null

    fun clone(): Pokemon {
        val pokemon = Pokemon()
        pokemon.id = id
        pokemon.name = name
        pokemon.weight = weight
        pokemon.weightCache = weightCache
        pokemon.evolution = evolution
        pokemon.stats = stats
        pokemon.types = types
        pokemon.sprites = sprites
        pokemon.species = species
        pokemon.image = image
        return pokemon
    }
}