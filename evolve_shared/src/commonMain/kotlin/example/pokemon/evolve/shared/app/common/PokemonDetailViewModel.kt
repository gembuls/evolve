package example.pokemon.evolve.shared.app.common

import example.pokemon.core.shared.app.common.BaseViewModel
import example.pokemon.evolve.shared.domain.evolve.entity.Berry
import example.pokemon.evolve.shared.domain.evolve.entity.Pokemon
import example.pokemon.evolve.shared.domain.evolve.entity.PokemonEvolution
import example.pokemon.evolve.shared.domain.evolve.usecase.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class PokemonDetailViewModel : KoinComponent, BaseViewModel() {
    private val getPokemonsLocalUseCase: GetPokemonsLocalUseCase by inject()
    private val setPokemonsLocalUseCase: SetPokemonsLocalUseCase by inject()

    private val getPokemonDetailNetworkUseCase: GetPokemonDetailNetworkUseCase by inject()
    private val getPokemonDetailLocalUseCase: GetPokemonDetailLocalUseCase by inject()
    private val setPokemonDetailLocalUseCase: SetPokemonDetailLocalUseCase by inject()

    private val getBerriesNetworkUseCase: GetBerriesNetworkUseCase by inject()
    private val getBerriesLocalUseCase: GetBerriesLocalUseCase by inject()

    var id = 0
    var name = ""
    var pokemon = MutableStateFlow<Pokemon?>(null)
    var nextPokemon = MutableStateFlow<Pokemon?>(null)
    var berries = MutableStateFlow<MutableList<Berry>?>(null)
    var onUpdateParent = MutableStateFlow<String?>(null)
    var onEvolve = MutableStateFlow<String?>(null)

    fun getPokemonDetailLocal(nameArg: String? = null) {
        scope.launch {
            loadingIndicator.value = true
            val pokemonLocal = getPokemonDetailLocalUseCase(nameArg ?: name)
            pokemonLocal?.let {
                if (name.isEmpty() || name == it.name) pokemon.value = it
                else nextPokemon.value = it
                loadingIndicator.value = false
            } ?: run {
                getPokemonDetailNetwork(nameArg)
            }
        }
    }

    private fun getPokemonDetailNetwork(nameArg: String? = null) {
        scope.launch {
            loadingIndicator.value = true
            try {
                val response = getPokemonDetailNetworkUseCase(nameArg ?: name)
                response?.let {
                    loadingIndicator.value = false
                    setPokemonDetailLocalUseCase(it)
                    if (name.isEmpty() || name == it.name) pokemon.value = it
                    else nextPokemon.value = it
                }
            } catch (e: Exception) {
                e.printStackTrace()
                pokemon.value = null
            }
        }
    }

    fun getBerriesLocal() {
        scope.launch {
            val berriessLocal = getBerriesLocalUseCase()
            berriessLocal.takeIf { it.isNotEmpty() }?.let {
                berries.value = it.toMutableList()
                loadingIndicator.value = false
            } ?: run {
                getBerriesNetwork()
            }
        }
    }

    private fun getBerriesNetwork() {
        scope.launch {
            loadingIndicator.value = true
            try {
                val response = getBerriesNetworkUseCase(0, 100000)
                berries.value = response
                loadingIndicator.value = false
            } catch (e: Exception) {
                e.printStackTrace()
                berries.value = mutableListOf()
            }
        }
    }

    fun feed(berry: Berry) {
        scope.launch {
            loadingIndicator.value = true
            try {
                pokemon.value?.let {
                    it.types?.let { ts ->
                        val arrType = mutableListOf<String>()
                        for (t in ts) t.type?.name?.let { tv ->
                            arrType.add(tv)
                        }
                        updateWeight(it, berry , arrType)
                    }
                }
                loadingIndicator.value = false
            } catch (e: Exception) {
                e.printStackTrace()
                loadingIndicator.value = false
                errorMessage.value = e.message
            }
        }
    }

    private fun updateWeight(it: Pokemon, berry: Berry, arrType: MutableList<String>) {
        scope.launch {
            if (it.weight != null && berry.weight != null && nextPokemon.value?.weight != null) {
                var newWeight = it.weight!!
                if (berry.naturalGiftType?.name in arrType) {
                    val increase = newWeight + berry.weight!!
                    newWeight =
                        if (increase > nextPokemon.value?.weight!!) nextPokemon.value?.weight!! else increase
                    successMessage.value = "Yummy! I Like this! (+${berry.weight!!})"
                } else {
                    val decrease = newWeight - berry.weight!! * 2
                    if (decrease > 0) newWeight = decrease
                    errorMessage.value =
                        "Ugh! I don't like this! (-${berry.weight!! * 2})"
                }
                val temp = it.clone().apply { weight = newWeight }
                setPokemonDetailLocalUseCase(temp)
                pokemon.value = temp
            }
        }
    }

    fun add(nameArg: String? = null) {
        updateParent(nameArg, true)
    }

    fun evolve() {
        scope.launch {
            try {
                pokemon.value?.let {
                    setPokemonDetailLocalUseCase(it.clone().apply { weight = it.weightCache })
                    updateParent(isChosenArg = false, evolve = true)
                    updateParent(nextPokemon.value?.name, isChosenArg = true)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                loadingIndicator.value = false
                errorMessage.value = e.message
            }
        }
    }

    fun delete() {
        scope.launch {
            try {
                pokemon.value?.let {
                    setPokemonDetailLocalUseCase(it.clone().apply { weight = it.weightCache })
                    updateParent(isChosenArg = false)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                loadingIndicator.value = false
                errorMessage.value = e.message
            }
        }
    }

    private fun updateParent(
        nameArg: String? = null,
        isChosenArg: Boolean? = null,
        evolve: Boolean? = null
    ) {
        scope.launch {
            loadingIndicator.value = true
            try {
                val pokemonUrl = getPokemonsLocalUseCase(0, 1, nameArg ?: name).firstOrNull()
                pokemonUrl?.let { it ->
                    setPokemonsLocalUseCase(
                        mutableListOf(
                            it.clone().apply { isChosen = isChosenArg })
                    )
                }
                loadingIndicator.value = false
                if (evolve == true) {
                    successMessage.value = "Pokemon has been evolved to ${nextPokemon.value?.name}"
                    onEvolve.value = nextPokemon.value?.name
                } else {
                    successMessage.value =
                        "Pokemon has been ${if (isChosenArg == true) "saved to" else "deleted from"} collections."
                    onUpdateParent.value = nameArg ?: name
                }
            } catch (e: Exception) {
                e.printStackTrace()
                loadingIndicator.value = false
                errorMessage.value = e.message
            }
        }
    }

    fun checkEvolution(pokemonEvolution: PokemonEvolution?, name: String): String? {
        var nameNextPokemon: String? = null
        pokemonEvolution?.evolvesTo?.takeIf { it.isNotEmpty() }?.get(0)?.let { e ->
            e.species?.let { s ->
                val split = s.url!!.split("/")
                if (name != s.name && id > 0 && split[split.size - 2].toInt() > id) {
                    nameNextPokemon = s.name
                    getPokemonDetailLocal(s.name)
                } else checkEvolution(e, name)
            }
        }
        return nameNextPokemon
    }
}