package example.pokemon.evolve.shared.app.pokemonlist

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import com.google.android.material.chip.Chip
import example.pokemon.core.shared.app.common.BaseFragment
import example.pokemon.core.shared.external.constant.AppConstant
import example.pokemon.core.shared.external.extension.*
import example.pokemon.evolve.shared.R
import example.pokemon.evolve.shared.app.common.PokemonAdapter
import example.pokemon.evolve.shared.app.common.PokemonDetailViewModel
import example.pokemon.evolve.shared.app.common.PokemonStatAdapter
import example.pokemon.evolve.shared.databinding.PokemonListFragmentBinding
import example.pokemon.evolve.shared.domain.evolve.entity.Pokemon
import example.pokemon.evolve.shared.domain.evolve.entity.PokemonUrl
import example.pokemon.evolve.shared.external.utility.GridSpacingItemDecoration
import example.pokemon.evolve.shared.external.utility.ZoomImageView
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.math.roundToInt
import example.pokemon.core.shared.R as cR

class PokemonListFragment : BaseFragment<PokemonListFragmentBinding>(
    R.layout.pokemon_list_fragment
) {
    private val vm: PokemonListViewModel by viewModel()
    private val vmDetail: PokemonDetailViewModel by viewModel()

    private lateinit var pokemonAdapter: PokemonAdapter
    private lateinit var pokemonStatAdapter: PokemonStatAdapter
    private lateinit var autoComplete: ArrayAdapter<String>
    private var selected: Pair<View, String>? = null
    private var detailView: ZoomImageView? = null
    private var nextPokemonName: String? = null

    override fun showBottomNavBar() = sharedPreferences.getBoolean(AppConstant.MULTIPLE_KEY, false)
    override fun actionBarTitle() = getString(R.string.pokemon_list_title)
    override fun expandActionBar() = true
    override fun actionBarHeight() = 180

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupHeader()

        binding.lifecycleOwner = viewLifecycleOwner
        binding.vm = vm.also {
            it.loadingIndicator.launchAndCollectIn(this, Lifecycle.State.STARTED) { l ->
                onLoading(l)
            }
            it.successMessage.launchAndCollectIn(this, Lifecycle.State.STARTED) { m ->
                showSuccessSnackbar(m)
                it.successMessage.value = null
            }
            it.errorMessage.launchAndCollectIn(this, Lifecycle.State.STARTED) { m ->
                showErrorSnackbar(m)
                it.errorMessage.value = null
            }
            it.toastMessage.launchAndCollectIn(this, Lifecycle.State.STARTED) { m ->
                showToast(m)
                it.toastMessage.value = null
            }
            it.allPokemons.launchAndCollectIn(this, Lifecycle.State.STARTED) { aps ->
                val temp = mutableListOf<String>()
                for (item in aps) item.name?.takeIf { n -> n.isNotEmpty() }
                    ?.let { n -> temp.add(n) }
                if (temp.isNotEmpty()) {
                    autoComplete.clear()
                    autoComplete.addAll(temp)
                }
            }
            it.pokemons.launchAndCollectIn(this, Lifecycle.State.STARTED) { ps ->
                notifyAdapter(ps)
                it.pokemons.value = null
            }
            it.pokemon.launchAndCollectIn(this, Lifecycle.State.STARTED) { p ->
                notifyParentAdapter(p)
                it.pokemon.value = null
            }
        }

        binding.vmDetail = vmDetail.also {
            it.pokemon.launchAndCollectIn(this, Lifecycle.State.STARTED) { p ->
                p?.name?.let { n ->
                    if (it.name.isEmpty()) notifyItemAdapter(p)
                    else if (selected?.second == n) onSelected(selected?.first, p)
                }
            }
            it.nextPokemon.launchAndCollectIn(this, Lifecycle.State.STARTED) { p ->
                notifyNextPokemonDetail(p)
                it.nextPokemon.value = null
            }
            it.onUpdateParent.launchAndCollectIn(this, Lifecycle.State.STARTED) { n ->
                onChosen(n)
                it.onUpdateParent.value = null
            }
        }

        binding.srlPokemonList.setOnRefreshListener {
            load(0)
        }
        pokemonAdapter = PokemonAdapter(1.0, 130, 8, 2).apply {
            skeleton = PokemonUrl().apply { name = "" }
            onSelected = { v, i ->
                selected = Pair(v, i.name.toString())
                vmDetail.name = i.name.toString()
                if (i.pokemon == null) vmDetail.getPokemonDetailLocal(i.name)
                else vmDetail.pokemon.value = i.pokemon
            }
            fetchData = {
                vm.page++
                load(vm.page)
            }
        }
        binding.rvPokemon.apply {
            adapter = pokemonAdapter
            addOnScrollListener(pokemonAdapter.Listener(binding.srlPokemonList))
            addItemDecoration(GridSpacingItemDecoration(3, 10, false))
        }

        pokemonStatAdapter = PokemonStatAdapter(1.0, 40)
        binding.detail.rvPokemonDetailStat.apply {
            adapter = pokemonStatAdapter
            isNestedScrollingEnabled = false
            addItemDecoration(GridSpacingItemDecoration(2, 10, false))
        }
        binding.detail.rvPokemonDetailBerry.layoutParams.width = 1

        load(0)
    }

    private fun setupHeader() {
        actionBarCollapsingLayout()?.expandedTitleMarginBottom = dpToPx(125f)
        if (showBottomNavBar()) binding.clPokemonList.setPadding(0, 0, 0, dpToPx(60f))

        actionBarExpandedDescription()?.apply {
            isVisible = true
            text = getString(R.string.pokemon_list_description)
        }
        autoComplete = ArrayAdapter(requireContext(), android.R.layout.simple_dropdown_item_1line)
        actionBarExpandedAutoComplete()?.apply {
            isVisible = true
            setAdapter(autoComplete)
            setOnItemClickListener { _, _, position, _ ->
                vm.search = autoComplete.getItem(position) ?: ""
                load(0)
            }
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    vm.search = text.toString()
                    load(0)
                    return@setOnEditorActionListener true
                }
                false
            }
        }
    }

    private fun onLoading(isLoading: Boolean?) {
        if (pokemonAdapter.itemsCache.size == 0 && isLoading == true) {
            binding.rvPokemon.post { pokemonAdapter.showSkeletons() }
        }
    }

    private fun load(p: Int) {
        hideKeyboard()
        vm.page = p
        if (p == 0) binding.rvPokemon.post { pokemonAdapter.clear() }
        if (vm.allPokemons.value.isEmpty()) {
            vm.page = 0
            vm.getPokemonsLocal(100000)
        } else vm.getPokemonsLocal()
    }

    private fun notifyAdapter(pokemons: MutableList<PokemonUrl>?) {
        pokemons?.let { its ->
            binding.rvPokemon.post {
                pokemonAdapter.addItem {
                    pokemonAdapter.items.addAll(its)
                    pokemonAdapter.itemsCache.addAll(its)
                }
                for (it in its) {
                    if (it.pokemon == null) vmDetail.getPokemonDetailLocal(it.name.toString())
                }
            }
        }
        binding.srlPokemonList.isRefreshing = false
    }

    private fun notifyParentAdapter(pokemonUrl: PokemonUrl?) {
        pokemonUrl?.let { p ->
            val exist = pokemonAdapter.items.firstOrNull { i -> i.name == p.name }
            exist?.let { it ->
                val i = pokemonAdapter.items.indexOf(it)
                pokemonAdapter.items[i] = p.clone().apply { pokemon = it.pokemon }
                binding.rvPokemon.post { pokemonAdapter.notifyItemChanged(i) }
            }
        }
    }

    private fun notifyItemAdapter(pokemonArg: Pokemon?) {
        pokemonArg?.let { p ->
            val exist = pokemonAdapter.items.firstOrNull { i -> i.name == p.name }
            //vm.toastMessage.value = "notifyItemAdapter: ${p.name}: ${pokemonAdapter.items.size}: ${exist != null}: ${pokemonArg.weightCache}"
            exist?.let {
                val i = pokemonAdapter.items.indexOf(it)
                pokemonAdapter.items[i] = it.clone().apply { pokemon = p }
                binding.rvPokemon.post { pokemonAdapter.notifyItemChanged(i) }
            }
        }
    }

    private fun notifyItemDetail(pokemon: Pokemon?) {
        pokemon?.types?.takeIf { it.isNotEmpty() }?.let { its ->
            binding.detail.cgPokemonDetail.removeAllViews()
            for (it in its) {
                val bg = when (it.type?.name) {
                    "poison", "water", "steel", "ghost", "ice" -> cR.color.blueSoft
                    "grass", "bug" -> cR.color.greenLight
                    "fire", "fairy" -> cR.color.redDark
                    "flying", "ground" -> cR.color.mustardLight
                    "electric", "psychic", "dragon" -> cR.color.info
                    else -> cR.color.grey20
                }
                binding.detail.cgPokemonDetail.addView(Chip(requireContext()).apply {
                    text =
                        it.type?.name?.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
                    textAlignment = View.TEXT_ALIGNMENT_CENTER
                    isEnabled = false
                    id = View.generateViewId()
                    chipStrokeWidth = 0f
                    chipMinHeight = dpToPx(16f).toFloat()
                    setEnsureMinTouchTargetSize(false)
                    setChipBackgroundColorResource(bg)
                    setTextAppearanceResource(cR.style.Text_Poppins_Body1_Regular)
                    setPadding(0, 0, 0, 0)
                })
            }
        }

        pokemon?.stats?.takeIf { it.isNotEmpty() }?.let { its ->
            pokemonStatAdapter.clear()
            pokemonStatAdapter.addItem {
                pokemonStatAdapter.items = its
            }
        }
    }

    @Suppress("SetTextI18n")
    private fun notifyNextPokemonDetail(pokemon: Pokemon?) {
        pokemon?.takeIf { it.name == nextPokemonName }?.let { p ->
            binding.detail.tvPokemonDetailEvolution.text =
                p.name?.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
            if (p.weight != null && vmDetail.pokemon.value?.weight != null) {
                binding.detail.tvPokemonDetailEvolutionInfo.apply {
                    isVisible = true
                    text = "(${vmDetail.pokemon.value?.weight} / ${p.weight}) Kg"
                }
                val progress = vmDetail.pokemon.value?.weight!!.toFloat() / p.weight!!.toFloat()
                binding.detail.lpiPokemonDetail.setProgress((progress * 100).roundToInt(), true)
            }
            binding.detail.mbPokemonDetail.isVisible = true
            binding.detail.clPokemonDetailEvolution.isVisible = true
        }
    }

    private fun onSelected(view: View?, item: Pokemon?) {
        item?.let { p ->
            hideKeyboard()
            actionBarLayout()?.setExpanded(false)
            binding.detail.clPokemonDetailEvolution.isVisible = false
            binding.detail.tvPokemonDetailTitle.text =
                p.name?.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
            binding.detail.mbPokemonDetail.apply {
                isVisible = false
                text = getString(R.string.pokemon_detail_choose)
                setOnClickListener { vmDetail.add(p.name) }
            }
            view?.let { v ->
                detailView = ZoomImageView(
                    p.image.toString(),
                    binding.clPokemonList,
                    binding.detail.root,
                    v,
                    binding.detail.ivPokemonDetail,
                    binding.detail.mbPokemonDetailClose
                ) {
                    vmDetail.id = 0
                    vmDetail.name = ""
                    vmDetail.pokemon.value = null
                }
                detailView?.show()
            }

            notifyItemDetail(p)
            p.id?.let { vmDetail.id = it }
            nextPokemonName = vmDetail.checkEvolution(p.evolution, p.name.toString())
        }
    }

    private fun onChosen(name: String?) {
        name?.let {
            if (!showBottomNavBar()) {
                sharedPreferences.edit().putString(AppConstant.EVOLVING_KEY, it).apply()
                activity?.finish()
                startActivity(activity?.intent)
            } else {
                vm.getPokemonsLocal(searchArg = it)
                detailView?.hide()
            }
        }
    }
}