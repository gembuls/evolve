package example.pokemon.evolve.shared.app.common

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.internal.ViewUtils
import example.pokemon.core.shared.R
import example.pokemon.evolve.shared.databinding.PokemonStatItemBinding
import example.pokemon.evolve.shared.domain.evolve.entity.PokemonStat

class PokemonStatAdapter(
    private val widthRatio: Double = 1.0,
    private val height: Int = 0,
) : RecyclerView.Adapter<PokemonStatAdapter.ViewHolder>() {
    var items = mutableListOf<PokemonStat>()

    inner class ViewHolder(
        private val binding: PokemonStatItemBinding,
    ) : RecyclerView.ViewHolder(binding.root) {
        private fun isLoading(item: PokemonStat) = item.base_stat == 0

        @Suppress("kotlin:S1186")
        fun bind(item: PokemonStat) {
            val context = binding.root.context
            if (isLoading(item)) {
                val anim = AnimationUtils.loadAnimation(context, R.anim.left_right)
                binding.shine.isVisible = true
                binding.shine.startAnimation(anim)
                anim.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(p0: Animation?) {
                    }

                    override fun onAnimationEnd(p0: Animation?) {
                        binding.shine.startAnimation(anim)
                    }

                    override fun onAnimationRepeat(p0: Animation?) {
                    }

                })
            } else {
                binding.shine.isVisible = false
                binding.tvPokemonStatItemTitle.text = item.stat?.name?.replace("-", " ")
                binding.tvPokemonStatItem.text = item.base_stat.toString()
            }
        }
    }

    @Suppress("RestrictedApi")
    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val binding =
            PokemonStatItemBinding.inflate(
                LayoutInflater.from(viewGroup.context),
                viewGroup,
                false
            )
        if (widthRatio != 1.0) binding.root.layoutParams.width =
            (viewGroup.measuredWidth * widthRatio).toInt()
        if (height != 0) binding.root.layoutParams.height =
            ViewUtils.dpToPx(binding.root.context, height).toInt()
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(items[position])
    }

    override fun getItemCount() = items.size

    fun clear() {
        val size = items.size
        items = mutableListOf()
        notifyItemRangeRemoved(0, size)
    }

    fun addItem(process: () -> Unit) {
        if (items.size > 0) clear()
        val sizeBefore = items.size
        process.invoke()
        val sizeAfter = items.size
        notifyItemRangeInserted(sizeBefore, sizeAfter - 1)
    }
}