package example.pokemon.evolve.shared.app.collectionlist

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import example.pokemon.core.shared.app.common.BaseFragment
import example.pokemon.core.shared.external.constant.AppConstant
import example.pokemon.core.shared.external.extension.*
import example.pokemon.evolve.shared.R
import example.pokemon.evolve.shared.app.common.PokemonAdapter
import example.pokemon.evolve.shared.app.common.PokemonDetailViewModel
import example.pokemon.evolve.shared.app.pokemonlist.PokemonListViewModel
import example.pokemon.evolve.shared.databinding.CollectionsListFragmentBinding
import example.pokemon.evolve.shared.domain.evolve.entity.Pokemon
import example.pokemon.evolve.shared.domain.evolve.entity.PokemonUrl
import example.pokemon.evolve.shared.external.utility.GridSpacingItemDecoration
import org.koin.androidx.viewmodel.ext.android.viewModel
import example.pokemon.core.shared.R as cR

class CollectionListFragment :
    BaseFragment<CollectionsListFragmentBinding>(R.layout.collections_list_fragment) {
    private val vm: PokemonListViewModel by viewModel()
    private val vmDetail: PokemonDetailViewModel by viewModel()

    private lateinit var pokemonAdapter: PokemonAdapter
    private lateinit var autoComplete: ArrayAdapter<String>

    override fun showBottomNavBar() = sharedPreferences.getBoolean(AppConstant.MULTIPLE_KEY, false)
    override fun actionBarTitle() = getString(cR.string.menu_collections)
    override fun expandActionBar() = true
    override fun actionBarHeight() = 125

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        actionBarExpandedDescription()?.isVisible = false
        actionBarCollapsingLayout()?.expandedTitleMarginBottom = dpToPx(70f)
        if (showBottomNavBar()) binding.clCollections.setPadding(0, 0, 0, dpToPx(60f))

        autoComplete = ArrayAdapter(requireContext(), android.R.layout.simple_dropdown_item_1line)
        actionBarExpandedAutoComplete()?.apply {
            isVisible = true
            setAdapter(autoComplete)
            setOnItemClickListener { _, _, position, _ ->
                vm.search = autoComplete.getItem(position) ?: ""
                load(0)
            }
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    vm.search = text.toString()
                    load(0)
                    return@setOnEditorActionListener true
                }
                false
            }
        }

        binding.lifecycleOwner = viewLifecycleOwner
        binding.vm = vm.also {
            it.isChosen = true
            it.loadingIndicator.launchAndCollectIn(this, Lifecycle.State.STARTED) { l ->
                onLoading(l)
            }
            it.successMessage.launchAndCollectIn(this, Lifecycle.State.STARTED) { m ->
                showSuccessSnackbar(m)
                it.successMessage.value = null
            }
            it.errorMessage.launchAndCollectIn(this, Lifecycle.State.STARTED) { m ->
                showErrorSnackbar(m)
                it.errorMessage.value = null
            }
            it.toastMessage.launchAndCollectIn(this, Lifecycle.State.STARTED) { m ->
                showToast(m)
                it.toastMessage.value = null
            }
            it.allPokemons.launchAndCollectIn(this, Lifecycle.State.STARTED) { aps ->
                val temp = mutableListOf<String>()
                for (item in aps) item.name?.takeIf { n -> n.isNotEmpty() }
                    ?.let { n -> temp.add(n) }
                if (temp.isNotEmpty()) {
                    binding.tvEmpty.isVisible = false
                    autoComplete.clear()
                    autoComplete.addAll(temp)
                } else binding.tvEmpty.isVisible = true
            }
            it.pokemons.launchAndCollectIn(this, Lifecycle.State.STARTED) { ps ->
                notifyAdapter(ps)
                it.pokemons.value = null
            }
        }

        binding.vmDetail = vmDetail.also {
            it.pokemon.launchAndCollectIn(this, Lifecycle.State.STARTED) { p ->
                p?.name?.let { _ ->
                    if (it.name.isEmpty()) notifyItemAdapter(p)
                }
            }
        }

        binding.srlCollections.setOnRefreshListener {
            load(0)
        }
        pokemonAdapter = PokemonAdapter(1.0, 200, 8, 2).apply {
            enableFilter = false
            skeleton = PokemonUrl().apply { name = "" }
            onSelected = { _, i ->
                goTo(getString(R.string.route_pokemon_detail).replace("{name}", i.name.toString()))
            }
            fetchData = {
                vm.page++
                load(vm.page)
            }
        }
        binding.rvPokemonCollection.apply {
            adapter = pokemonAdapter
            addOnScrollListener(pokemonAdapter.Listener(binding.srlCollections))
            addItemDecoration(GridSpacingItemDecoration(2, 10, false))
        }

        load(0)
    }

    private fun onLoading(isLoading: Boolean?) {
        if (pokemonAdapter.itemsCache.size == 0 && isLoading == true) {
            binding.rvPokemonCollection.post { pokemonAdapter.showSkeletons() }
        }
    }

    private fun load(p: Int) {
        hideKeyboard()
        vm.page = p
        if (p == 0) binding.rvPokemonCollection.post { pokemonAdapter.clear() }
        if (vm.allPokemons.value.isEmpty()) {
            vm.page = 0
            vm.getPokemonsLocal(100000)
        } else vm.getPokemonsLocal()
    }

    private fun notifyAdapter(pokemons: MutableList<PokemonUrl>?) {
        pokemons?.let { its ->
            binding.rvPokemonCollection.post {
                pokemonAdapter.addItem {
                    pokemonAdapter.items.addAll(its)
                    pokemonAdapter.itemsCache.addAll(its)
                }
                for (it in its) {
                    if (it.pokemon == null) vmDetail.getPokemonDetailLocal(it.name.toString())
                }
            }
        }
        binding.srlCollections.isRefreshing = false
    }

    private fun notifyItemAdapter(pokemonArg: Pokemon?) {
        pokemonArg?.let { p ->
            val exist = pokemonAdapter.items.firstOrNull { i -> i.name == p.name }
            exist?.let { it ->
                val i = pokemonAdapter.items.indexOf(it)
                pokemonAdapter.items[i] = it.clone().apply { pokemon = p }
                binding.rvPokemonCollection.post { pokemonAdapter.notifyItemChanged(i) }
            }
        }
    }
}