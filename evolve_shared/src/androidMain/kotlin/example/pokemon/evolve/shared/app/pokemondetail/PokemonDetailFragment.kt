package example.pokemon.evolve.shared.app.pokemondetail

import android.content.ClipDescription.MIMETYPE_TEXT_PLAIN
import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.core.util.component1
import androidx.core.util.component2
import androidx.core.view.isVisible
import androidx.draganddrop.DropHelper
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.google.android.material.chip.Chip
import example.pokemon.core.shared.app.common.BaseFragment
import example.pokemon.core.shared.external.constant.AppConstant
import example.pokemon.core.shared.external.extension.*
import example.pokemon.evolve.shared.R
import example.pokemon.evolve.shared.app.common.PokemonDetailViewModel
import example.pokemon.evolve.shared.app.common.PokemonStatAdapter
import example.pokemon.evolve.shared.databinding.PokemonDetailFragmentBinding
import example.pokemon.evolve.shared.domain.evolve.entity.Berry
import example.pokemon.evolve.shared.domain.evolve.entity.Pokemon
import example.pokemon.evolve.shared.external.utility.GridSpacingItemDecoration
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.math.roundToInt
import example.pokemon.core.shared.R as cR

class PokemonDetailFragment : BaseFragment<PokemonDetailFragmentBinding>(
    R.layout.pokemon_detail_fragment
) {
    private val vm: PokemonDetailViewModel by viewModel()
    private lateinit var pokemonStatAdapter: PokemonStatAdapter
    private lateinit var berriesAdapter: BerryAdapter

    override fun actionBarTitle() = getString(R.string.pokemon_detail_title)
    override fun showActionBar() = sharedPreferences.getBoolean(AppConstant.MULTIPLE_KEY, false)
    override fun showBottomNavBar() = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(AppConstant.DELETED_KEY) { _, b ->
            if (b.getBoolean(AppConstant.DELETED_KEY)) vm.delete()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            vm.name = it.getString("name", "")
        }
        setupObserver()

        pokemonStatAdapter = PokemonStatAdapter(1.0, 40)
        binding.detail.rvPokemonDetailStat.apply {
            isNestedScrollingEnabled = false
            adapter = pokemonStatAdapter
            addItemDecoration(GridSpacingItemDecoration(2, 10, false))
        }

        berriesAdapter = BerryAdapter(1.0, 40)
        binding.detail.rvPokemonDetailBerry.apply {
            isNestedScrollingEnabled = false
            adapter = berriesAdapter
            addItemDecoration(object : ItemDecoration() {
                override fun getItemOffsets(
                    outRect: Rect,
                    view: View,
                    parent: RecyclerView,
                    state: RecyclerView.State
                ) {
                    val itemPosition = parent.getChildAdapterPosition(view)
                    if (itemPosition == RecyclerView.NO_POSITION) return

                    val itemCount = state.itemCount

                    if (itemPosition == 0) outRect.set(0, 5, 0, 0)
                    else if (itemCount > 0 && itemPosition == itemCount - 1) outRect.set(0, 0, 0, 5)
                    else outRect.set(0, 5, 0, 5)
                }
            })
        }

        DropHelper.configureView(
            requireActivity(),
            binding.detail.ivPokemonDetail,
            arrayOf(MIMETYPE_TEXT_PLAIN),
            DropHelper.Options.Builder()
                .setHighlightColor(ContextCompat.getColor(requireContext(), cR.color.grey10))
                .setHighlightCornerRadiusPx(dpToPx(16f))
                .build()
        ) { _, payload ->
            val item = payload.clip.getItemAt(0)
            val (_, remaining) = payload.partition { it == item }
            val berry = berriesAdapter.items[item.text.toString().toInt()]
            val arrType = mutableListOf<String>()
            for (t in vm.pokemon.value!!.types!!) t.type?.name?.let { tv ->
                arrType.add(tv)
            }
            vm.feed(berry)
            remaining
        }

        binding.detail.ivPokemonDetailBerryUp.setOnClickListener {
            val lm = binding.detail.rvPokemonDetailBerry.layoutManager as LinearLayoutManager
            val position = lm.findFirstCompletelyVisibleItemPosition()
            if (position > 0) lm.scrollToPosition(position - 1)
        }

        binding.detail.ivPokemonDetailBerryDown.setOnClickListener {
            val lm = binding.detail.rvPokemonDetailBerry.layoutManager as LinearLayoutManager
            val position = lm.findLastCompletelyVisibleItemPosition()
            if (position < (berriesAdapter.itemCount - 1)) {
                lm.scrollToPosition(position + 1)
            }
        }

        binding.detail.mbPokemonDetailClose.setOnClickListener {
            goTo(getString(R.string.route_delete_dialog).replace("{name}", vm.name))
        }
        vm.getPokemonDetailLocal()
    }

    private fun setupObserver() {
        binding.lifecycleOwner = this
        binding.vm = vm.also {
            if (it.name.isEmpty()) it.name =
                sharedPreferences.getString(AppConstant.EVOLVING_KEY, "").toString()
            it.loadingIndicator.launchAndCollectIn(this, Lifecycle.State.STARTED) { l ->
                showFullLoading(l)
            }
            it.successMessage.launchAndCollectIn(this, Lifecycle.State.STARTED) { m ->
                showSuccessSnackbar(m)
                it.successMessage.value = null
            }
            it.errorMessage.launchAndCollectIn(this, Lifecycle.State.STARTED) { m ->
                showErrorSnackbar(m)
                it.errorMessage.value = null
            }
            it.toastMessage.launchAndCollectIn(this, Lifecycle.State.STARTED) { m ->
                showToast(m)
                it.toastMessage.value = null
            }
            it.pokemon.launchAndCollectIn(this, Lifecycle.State.STARTED) { p ->
                notifyPokemonDetail(p)
            }
            it.nextPokemon.launchAndCollectIn(this, Lifecycle.State.STARTED) { p ->
                notifyNextPokemonDetail(p)
            }
            it.onEvolve.launchAndCollectIn(this, Lifecycle.State.STARTED) { n ->
                n?.let {
                    val isMultiple = sharedPreferences.getBoolean(AppConstant.MULTIPLE_KEY, false)
                    if (!isMultiple) sharedPreferences.edit().putString(AppConstant.EVOLVING_KEY, n)
                        .apply()
                }
                it.onEvolve.value = null
            }
            it.onUpdateParent.launchAndCollectIn(this, Lifecycle.State.STARTED) { n ->
                n?.let {
                    if (!sharedPreferences.getBoolean(AppConstant.MULTIPLE_KEY, false)) {
                        Handler(Looper.getMainLooper()).postDelayed({
                            activity?.finish()
                            startActivity(activity?.intent)
                        }, 1000)
                    } else findNavController().popBackStack()
                }
                it.onUpdateParent.value = null
            }
            it.berries.launchAndCollectIn(this, Lifecycle.State.STARTED) { bs ->
                notifyBerries(bs)
            }
        }
    }

    private fun notifyPokemonDetail(pokemon: Pokemon?) {
        pokemon?.let { p ->
            binding.detail.tvPokemonDetailTitle.text =
                p.name?.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
            p.image?.let { f ->
                binding.detail.ivPokemonDetail.loadImage(f)
            }
            setupTypes(p)
            if (pokemonStatAdapter.items.isEmpty()) {
                p.stats?.takeIf { it.isNotEmpty() }?.let { its ->
                    pokemonStatAdapter.clear()
                    pokemonStatAdapter.addItem {
                        pokemonStatAdapter.items = its
                    }
                }
            }
            p.id?.let { vm.id = it }
            p.types?.let { ts ->
                val arrType = mutableListOf<String>()
                for (t in ts) t.type?.name?.let { arrType.add(it) }
                berriesAdapter.types = arrType
            }
            vm.checkEvolution(p.evolution, p.name.toString())
        }
    }

    private fun notifyNextPokemonDetail(pokemon: Pokemon?) {
        pokemon?.let { p ->
            if (p.weight == (vm.pokemon.value?.weight ?: 0)) {
                binding.detail.rvPokemonDetailBerry.visibility = View.INVISIBLE
                binding.detail.ivPokemonDetailBerryUp.isVisible = false
                binding.detail.ivPokemonDetailBerryDown.isVisible = false
                binding.detail.mbPokemonDetail.apply {
                    isVisible = true
                    text = getString(R.string.pokemon_detail_evolve)
                    setOnClickListener {
                        vm.evolve()
                        isEnabled = false
                        binding.detail.mbPokemonDetailClose.isEnabled = false
                    }
                }
                startShine()
            } else if (berriesAdapter.items.isEmpty()) vm.getBerriesLocal()

            binding.detail.tvPokemonDetailEvolution.text =
                p.name?.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
            notifyProgress(vm.pokemon.value?.weight)
            binding.detail.clPokemonDetailEvolution.isVisible = true
        } ?: run {
            binding.detail.rvPokemonDetailBerry.layoutParams.width = 1
        }
    }

    @Suppress("SetTextI18n")
    private fun notifyProgress(current: Int?) {
        if (current != null && vm.nextPokemon.value?.weight != null) {
            binding.detail.tvPokemonDetailEvolutionInfo.apply {
                isVisible = true
                text = "($current / ${vm.nextPokemon.value?.weight}) Kg"
            }
            val progress = current.toFloat() / vm.nextPokemon.value?.weight!!.toFloat()
            binding.detail.lpiPokemonDetail.setProgress((progress * 100).roundToInt(), true)
        }
    }

    private fun notifyBerries(berries: MutableList<Berry>?) {
        berries?.takeIf { it.isNotEmpty() }?.let { bs ->
            berriesAdapter.clear()
            berriesAdapter.addItem {
                berriesAdapter.items.addAll(bs)
            }
            binding.detail.rvPokemonDetailBerry.isVisible = true
            binding.detail.ivPokemonDetailBerryUp.isVisible = true
            binding.detail.ivPokemonDetailBerryDown.isVisible = true
        }
    }

    private fun setupTypes(pokemon: Pokemon) {
        pokemon.types?.takeIf { it.isNotEmpty() }?.let { its ->
            binding.detail.cgPokemonDetail.removeAllViews()
            for (it in its) {
                val bg = when (it.type?.name) {
                    "poison", "water", "steel", "ghost", "ice" -> example.pokemon.core.shared.R.color.blueSoft
                    "grass", "bug" -> example.pokemon.core.shared.R.color.greenLight
                    "fire", "fairy" -> example.pokemon.core.shared.R.color.redDark
                    "flying", "ground" -> example.pokemon.core.shared.R.color.mustardLight
                    "electric", "psychic", "dragon" -> example.pokemon.core.shared.R.color.info
                    else -> example.pokemon.core.shared.R.color.grey20
                }
                binding.detail.cgPokemonDetail.addView(Chip(requireContext()).apply {
                    text = it.type?.name?.replaceFirstChar {
                        if (it.isLowerCase()) it.titlecase(
                            Locale.getDefault()
                        ) else it.toString()
                    }
                    textAlignment = View.TEXT_ALIGNMENT_CENTER
                    isEnabled = false
                    id = View.generateViewId()
                    chipStrokeWidth = 0f
                    chipMinHeight = dpToPx(16f).toFloat()
                    setEnsureMinTouchTargetSize(false)
                    setChipBackgroundColorResource(bg)
                    setTextAppearanceResource(example.pokemon.core.shared.R.style.Text_Poppins_Body1_Regular)
                    setPadding(0, 0, 0, 0)
                })
            }
        }
    }

    @Suppress("kotlin:S1186")
    private fun startShine() {
        val anim = AnimationUtils.loadAnimation(context, R.anim.bottom_top)
        binding.detail.shine.isVisible = true
        binding.detail.shine.startAnimation(anim)
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {
            }

            override fun onAnimationEnd(p0: Animation?) {
                binding.detail.shine.startAnimation(anim)
            }

            override fun onAnimationRepeat(p0: Animation?) {
            }

        })
    }
}