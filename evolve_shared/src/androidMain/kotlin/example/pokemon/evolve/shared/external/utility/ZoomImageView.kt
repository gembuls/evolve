package example.pokemon.evolve.shared.external.utility

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.graphics.Point
import android.graphics.Rect
import android.graphics.RectF
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import androidx.core.view.isVisible
import example.pokemon.core.shared.external.extension.loadImage

class ZoomImageView(
    private val imageUrl: String,
    outerContainerView: View,
    private val innerContainerView: View,
    private val thumbView: View,
    private val targetView: ImageView,
    private val closeView: View,
    private val onClose: () -> Unit,
) {
    private var currentAnimator: Animator? = null
    private var zoomDuration: Int = 250

    private var startScale = 0f
    private val startBoundsInt = Rect()
    private val finalBoundsInt = Rect()
    private val globalOffset = Point()
    private var startBounds: RectF
    private var finalBounds: RectF

    init {
        thumbView.getGlobalVisibleRect(startBoundsInt)
        outerContainerView.getGlobalVisibleRect(finalBoundsInt, globalOffset)
        startBoundsInt.offset(-globalOffset.x, -globalOffset.y)
        finalBoundsInt.offset(-globalOffset.x, -globalOffset.y)

        startBounds = RectF(startBoundsInt)
        finalBounds = RectF(finalBoundsInt)
    }

    fun show() {
        currentAnimator?.cancel()
        targetView.loadImage(imageUrl)

        if ((finalBounds.width() / finalBounds.height() > startBounds.width() / startBounds.height())) {
            startScale = startBounds.height() / finalBounds.height()
            val startWidth: Float = startScale * finalBounds.width()
            val deltaWidth: Float = (startWidth - startBounds.width()) / 2
            startBounds.left -= deltaWidth.toInt()
            startBounds.right += deltaWidth.toInt()
        } else {
            startScale = startBounds.width() / finalBounds.width()
            val startHeight: Float = startScale * finalBounds.height()
            val deltaHeight: Float = (startHeight - startBounds.height()) / 2f
            startBounds.top -= deltaHeight.toInt()
            startBounds.bottom += deltaHeight.toInt()
        }

        thumbView.alpha = 0f
        innerContainerView.isVisible = true

        targetView.pivotX = 0f
        targetView.pivotY = 0f

        currentAnimator = AnimatorSet().apply {
            play(
                ObjectAnimator.ofFloat(
                    targetView,
                    View.X,
                    startBounds.left,
                    finalBounds.left
                )
            ).apply {
                with(
                    ObjectAnimator.ofFloat(
                        targetView,
                        View.Y,
                        startBounds.top,
                        finalBounds.top
                    )
                )
                with(ObjectAnimator.ofFloat(targetView, View.SCALE_X, startScale, 1f))
                with(ObjectAnimator.ofFloat(targetView, View.SCALE_Y, startScale, 1f))
            }
            duration = zoomDuration.toLong()
            interpolator = DecelerateInterpolator()
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    currentAnimator = null
                }

                override fun onAnimationCancel(animation: Animator) {
                    currentAnimator = null
                }
            })
            start()
        }

        closeView.setOnClickListener {
            hide()
        }
    }

    fun hide() {
        currentAnimator?.cancel()
        currentAnimator = AnimatorSet().apply {
            play(
                ObjectAnimator.ofFloat(
                    targetView,
                    View.X,
                    startBounds.left
                )
            ).apply {
                with(ObjectAnimator.ofFloat(targetView, View.Y, startBounds.top))
                with(ObjectAnimator.ofFloat(targetView, View.SCALE_X, startScale))
                with(ObjectAnimator.ofFloat(targetView, View.SCALE_Y, startScale))
            }
            duration = zoomDuration.toLong()
            interpolator = DecelerateInterpolator()
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    hide()
                }

                override fun onAnimationCancel(animation: Animator) {
                    hide()
                }

                fun hide() {
                    thumbView.alpha = 1f
                    innerContainerView.isVisible = false
                    currentAnimator = null
                    onClose.invoke()
                }
            })
            start()
        }
    }
}