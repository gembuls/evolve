//[example_android](../../../index.md)/[example.pokemon.evolve.example.app](../index.md)/[MainActivity](index.md)/[topLevelDestinations](top-level-destinations.md)

# topLevelDestinations

[androidJvm]\

public [Set](https://developer.android.com/reference/kotlin/java/util/Set.html)&lt;[Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)&gt;[topLevelDestinations](top-level-destinations.md)()
