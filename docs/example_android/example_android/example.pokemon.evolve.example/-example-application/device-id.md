//[example_android](../../../index.md)/[example.pokemon.evolve.example](../index.md)/[ExampleApplication](index.md)/[deviceId](device-id.md)

# deviceId

[androidJvm]\

public [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[deviceId](device-id.md)()
