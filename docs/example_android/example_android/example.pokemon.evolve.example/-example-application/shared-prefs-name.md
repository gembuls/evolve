//[example_android](../../../index.md)/[example.pokemon.evolve.example](../index.md)/[ExampleApplication](index.md)/[sharedPrefsName](shared-prefs-name.md)

# sharedPrefsName

[androidJvm]\

public [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[sharedPrefsName](shared-prefs-name.md)()
