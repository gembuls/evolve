//[evolve_shared](../../index.md)/[example.pokemon.evolve.shared.domain.evolve](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [EvolveRepository](-evolve-repository/index.md) | [common]<br>public interface [EvolveRepository](-evolve-repository/index.md) |
