//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve](../index.md)/[EvolveRepository](index.md)/[getPokemonDetailNetwork](get-pokemon-detail-network.md)

# getPokemonDetailNetwork

[common]\

public abstract [Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)[getPokemonDetailNetwork](get-pokemon-detail-network.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)name)
