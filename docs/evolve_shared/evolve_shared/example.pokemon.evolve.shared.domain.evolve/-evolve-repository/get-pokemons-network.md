//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve](../index.md)/[EvolveRepository](index.md)/[getPokemonsNetwork](get-pokemons-network.md)

# getPokemonsNetwork

[common]\

public abstract [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;[getPokemonsNetwork](get-pokemons-network.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)offset, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)limit)
