//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve](../index.md)/[EvolveRepository](index.md)/[getBerriesNetwork](get-berries-network.md)

# getBerriesNetwork

[common]\

public abstract [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[Berry](../../example.pokemon.evolve.shared.domain.evolve.entity/-berry/index.md)&gt;[getBerriesNetwork](get-berries-network.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)offset, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)limit)
