//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve](../index.md)/[EvolveRepository](index.md)/[getPokemonDetailLocal](get-pokemon-detail-local.md)

# getPokemonDetailLocal

[common]\

public abstract [Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)[getPokemonDetailLocal](get-pokemon-detail-local.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)name)
