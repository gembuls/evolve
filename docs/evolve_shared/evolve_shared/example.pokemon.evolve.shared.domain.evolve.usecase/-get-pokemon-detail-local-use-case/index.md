//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.usecase](../index.md)/[GetPokemonDetailLocalUseCase](index.md)

# GetPokemonDetailLocalUseCase

[common]\
public final class [GetPokemonDetailLocalUseCase](index.md) implements KoinComponent

## Constructors

| | |
|---|---|
| [GetPokemonDetailLocalUseCase](-get-pokemon-detail-local-use-case.md) | [common]<br>public [GetPokemonDetailLocalUseCase](index.md)[GetPokemonDetailLocalUseCase](-get-pokemon-detail-local-use-case.md)() |

## Functions

| Name | Summary |
|---|---|
| [invoke](invoke.md) | [common]<br>public final [Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)[invoke](invoke.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)name) |
