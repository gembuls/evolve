//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.usecase](../index.md)/[GetBerriesNetworkUseCase](index.md)

# GetBerriesNetworkUseCase

[common]\
public final class [GetBerriesNetworkUseCase](index.md) implements KoinComponent

## Constructors

| | |
|---|---|
| [GetBerriesNetworkUseCase](-get-berries-network-use-case.md) | [common]<br>public [GetBerriesNetworkUseCase](index.md)[GetBerriesNetworkUseCase](-get-berries-network-use-case.md)() |

## Functions

| Name | Summary |
|---|---|
| [invoke](invoke.md) | [common]<br>public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[Berry](../../example.pokemon.evolve.shared.domain.evolve.entity/-berry/index.md)&gt;[invoke](invoke.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)offset, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)limit) |
