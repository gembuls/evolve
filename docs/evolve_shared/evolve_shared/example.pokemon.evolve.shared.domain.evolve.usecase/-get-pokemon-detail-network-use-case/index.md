//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.usecase](../index.md)/[GetPokemonDetailNetworkUseCase](index.md)

# GetPokemonDetailNetworkUseCase

[common]\
public final class [GetPokemonDetailNetworkUseCase](index.md) implements KoinComponent

## Constructors

| | |
|---|---|
| [GetPokemonDetailNetworkUseCase](-get-pokemon-detail-network-use-case.md) | [common]<br>public [GetPokemonDetailNetworkUseCase](index.md)[GetPokemonDetailNetworkUseCase](-get-pokemon-detail-network-use-case.md)() |

## Functions

| Name | Summary |
|---|---|
| [invoke](invoke.md) | [common]<br>public final [Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)[invoke](invoke.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)name) |
