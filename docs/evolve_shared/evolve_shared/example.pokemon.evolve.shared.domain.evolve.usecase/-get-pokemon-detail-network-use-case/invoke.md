//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.usecase](../index.md)/[GetPokemonDetailNetworkUseCase](index.md)/[invoke](invoke.md)

# invoke

[common]\

public final [Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)[invoke](invoke.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)name)
