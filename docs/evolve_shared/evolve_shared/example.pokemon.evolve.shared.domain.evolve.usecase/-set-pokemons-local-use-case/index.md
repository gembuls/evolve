//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.usecase](../index.md)/[SetPokemonsLocalUseCase](index.md)

# SetPokemonsLocalUseCase

[common]\
public final class [SetPokemonsLocalUseCase](index.md) implements KoinComponent

## Constructors

| | |
|---|---|
| [SetPokemonsLocalUseCase](-set-pokemons-local-use-case.md) | [common]<br>public [SetPokemonsLocalUseCase](index.md)[SetPokemonsLocalUseCase](-set-pokemons-local-use-case.md)() |

## Functions

| Name | Summary |
|---|---|
| [invoke](invoke.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[invoke](invoke.md)([List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;pokemons) |
