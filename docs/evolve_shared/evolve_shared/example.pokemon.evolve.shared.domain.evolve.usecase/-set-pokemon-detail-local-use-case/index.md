//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.usecase](../index.md)/[SetPokemonDetailLocalUseCase](index.md)

# SetPokemonDetailLocalUseCase

[common]\
public final class [SetPokemonDetailLocalUseCase](index.md) implements KoinComponent

## Constructors

| | |
|---|---|
| [SetPokemonDetailLocalUseCase](-set-pokemon-detail-local-use-case.md) | [common]<br>public [SetPokemonDetailLocalUseCase](index.md)[SetPokemonDetailLocalUseCase](-set-pokemon-detail-local-use-case.md)() |

## Functions

| Name | Summary |
|---|---|
| [invoke](invoke.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[invoke](invoke.md)([Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)pokemon) |
