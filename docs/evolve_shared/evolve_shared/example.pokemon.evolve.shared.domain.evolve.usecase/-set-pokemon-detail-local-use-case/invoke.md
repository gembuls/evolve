//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.usecase](../index.md)/[SetPokemonDetailLocalUseCase](index.md)/[invoke](invoke.md)

# invoke

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[invoke](invoke.md)([Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)pokemon)
