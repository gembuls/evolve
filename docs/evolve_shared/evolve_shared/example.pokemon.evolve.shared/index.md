//[evolve_shared](../../index.md)/[example.pokemon.evolve.shared](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [EvolveKoinKt](-evolve-koin-kt/index.md) | [android, common, ios]<br>[android, common, ios]<br>public final class [EvolveKoinKt](-evolve-koin-kt/index.md) |
| [ExpectKt](-expect-kt/index.md) | [android, common, ios]<br>[android, common, ios]<br>public final class [ExpectKt](-expect-kt/index.md) |
