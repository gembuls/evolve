//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared](../index.md)/[EvolveKoinKt](index.md)

# EvolveKoinKt

[android, common, ios]\
public final class [EvolveKoinKt](index.md)

## Functions

| Name | Summary |
|---|---|
| [initKoin](init-koin.md) | [common]<br>public final static KoinApplication[initKoin](init-koin.md)(Contextcontext, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)host, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)deviceId, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)version) |
| [prefsNameShared](prefs-name-shared.md) | [common]<br>public final static [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[prefsNameShared](prefs-name-shared.md)() |
| [protocolShared](protocol-shared.md) | [common]<br>public final static URLProtocol[protocolShared](protocol-shared.md)() |
| [provideStockDb](provide-stock-db.md) | [common]<br>public final static Realm[provideStockDb](provide-stock-db.md)() |
