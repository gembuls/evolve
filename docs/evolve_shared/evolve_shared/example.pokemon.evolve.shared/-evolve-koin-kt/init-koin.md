//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared](../index.md)/[EvolveKoinKt](index.md)/[initKoin](init-koin.md)

# initKoin

[common]\

public final static KoinApplication[initKoin](init-koin.md)(Contextcontext, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)host, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)deviceId, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)version)
