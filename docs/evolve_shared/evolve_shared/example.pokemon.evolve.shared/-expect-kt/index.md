//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared](../index.md)/[ExpectKt](index.md)

# ExpectKt

[android, common, ios]\
public final class [ExpectKt](index.md)

## Functions

| Name | Summary |
|---|---|
| [libModule](lib-module.md) | [common, android, ios]<br>[common, android, ios]<br>public final static Module[libModule](lib-module.md)() |
