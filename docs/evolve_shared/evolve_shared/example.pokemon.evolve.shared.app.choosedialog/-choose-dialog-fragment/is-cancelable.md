//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.choosedialog](../index.md)/[ChooseDialogFragment](index.md)/[isCancelable](is-cancelable.md)

# isCancelable

[android]\

public [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[isCancelable](is-cancelable.md)()
