//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.onboarding](../index.md)/[OnBoardingViewModel](index.md)/[getOnNext](get-on-next.md)

# getOnNext

[common]\

public final MutableStateFlow&lt;[Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)&gt;[getOnNext](get-on-next.md)()
