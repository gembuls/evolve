//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.onboarding](../index.md)/[OnBoardingFragment](index.md)

# OnBoardingFragment

[android]\
public final class [OnBoardingFragment](index.md) extends BaseFragment&lt;&lt;Error class: unknown class&gt;&gt;

## Constructors

| | |
|---|---|
| [OnBoardingFragment](-on-boarding-fragment.md) | [android]<br>public [OnBoardingFragment](index.md)[OnBoardingFragment](-on-boarding-fragment.md)() |

## Functions

| Name | Summary |
|---|---|
| [onCreate](on-create.md) | [android]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onCreate](on-create.md)([Bundle](https://developer.android.com/reference/kotlin/android/os/Bundle.html)savedInstanceState) |
| [onViewCreated](on-view-created.md) | [android]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onViewCreated](on-view-created.md)([View](https://developer.android.com/reference/kotlin/android/view/View.html)view, [Bundle](https://developer.android.com/reference/kotlin/android/os/Bundle.html)savedInstanceState) |
