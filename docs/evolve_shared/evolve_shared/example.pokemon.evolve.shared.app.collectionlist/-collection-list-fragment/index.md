//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.collectionlist](../index.md)/[CollectionListFragment](index.md)

# CollectionListFragment

[android]\
public final class [CollectionListFragment](index.md) extends BaseFragment&lt;&lt;Error class: unknown class&gt;&gt;

## Constructors

| | |
|---|---|
| [CollectionListFragment](-collection-list-fragment.md) | [android]<br>public [CollectionListFragment](index.md)[CollectionListFragment](-collection-list-fragment.md)() |

## Functions

| Name | Summary |
|---|---|
| [expandActionBar](expand-action-bar.md) | [android]<br>public [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[expandActionBar](expand-action-bar.md)() |
| [onViewCreated](on-view-created.md) | [android]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onViewCreated](on-view-created.md)([View](https://developer.android.com/reference/kotlin/android/view/View.html)view, [Bundle](https://developer.android.com/reference/kotlin/android/os/Bundle.html)savedInstanceState) |
