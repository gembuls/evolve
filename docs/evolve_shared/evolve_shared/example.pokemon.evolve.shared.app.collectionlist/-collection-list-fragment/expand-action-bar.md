//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.collectionlist](../index.md)/[CollectionListFragment](index.md)/[expandActionBar](expand-action-bar.md)

# expandActionBar

[android]\

public [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[expandActionBar](expand-action-bar.md)()
