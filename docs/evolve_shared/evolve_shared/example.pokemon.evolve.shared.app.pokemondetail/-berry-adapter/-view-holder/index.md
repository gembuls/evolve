//[evolve_shared](../../../../index.md)/[example.pokemon.evolve.shared.app.pokemondetail](../../index.md)/[BerryAdapter](../index.md)/[ViewHolder](index.md)

# ViewHolder

[android]\
public final class [ViewHolder](index.md) extends [RecyclerView.ViewHolder](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.ViewHolder.html)

## Constructors

| | |
|---|---|
| [BerryAdapter.ViewHolder](-berry-adapter.-view-holder.md) | [android]<br>public [BerryAdapter.ViewHolder](index.md)[BerryAdapter.ViewHolder](-berry-adapter.-view-holder.md)(&lt;Error class: unknown class&gt;binding) |

## Functions

| Name | Summary |
|---|---|
| [bind](bind.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[bind](bind.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)position) |
