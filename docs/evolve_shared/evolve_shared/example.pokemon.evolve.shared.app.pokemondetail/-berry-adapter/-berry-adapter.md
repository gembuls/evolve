//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.pokemondetail](../index.md)/[BerryAdapter](index.md)/[BerryAdapter](-berry-adapter.md)

# BerryAdapter

[android]\

public [BerryAdapter](index.md)[BerryAdapter](-berry-adapter.md)([Double](https://developer.android.com/reference/kotlin/java/lang/Double.html)widthRatio, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)height)
