//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.pokemondetail](../index.md)/[BerryAdapter](index.md)

# BerryAdapter

[android]\
public final class [BerryAdapter](index.md) extends [RecyclerView.Adapter](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.Adapter.html)&lt;[BerryAdapter.ViewHolder](-view-holder/index.md)&gt;

## Constructors

| | |
|---|---|
| [BerryAdapter](-berry-adapter.md) | [android]<br>public [BerryAdapter](index.md)[BerryAdapter](-berry-adapter.md)([Double](https://developer.android.com/reference/kotlin/java/lang/Double.html)widthRatio, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)height) |

## Types

| Name | Summary |
|---|---|
| [ViewHolder](-view-holder/index.md) | [android]<br>public final class [ViewHolder](-view-holder/index.md) extends [RecyclerView.ViewHolder](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.ViewHolder.html) |

## Functions

| Name | Summary |
|---|---|
| [addItem](add-item.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[addItem](add-item.md)(Function0&lt;[Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)&gt;process) |
| [clear](clear.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[clear](clear.md)() |
| [getItemCount](get-item-count.md) | [android]<br>public [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getItemCount](get-item-count.md)() |
| [getItems](get-items.md) | [android]<br>public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;Berry&gt;[getItems](get-items.md)() |
| [getTypes](get-types.md) | [android]<br>public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[String](https://developer.android.com/reference/kotlin/java/lang/String.html)&gt;[getTypes](get-types.md)() |
| [onBindViewHolder](on-bind-view-holder.md) | [android]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onBindViewHolder](on-bind-view-holder.md)([BerryAdapter.ViewHolder](-view-holder/index.md)viewHolder, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)position) |
| [onCreateViewHolder](on-create-view-holder.md) | [android]<br>public [BerryAdapter.ViewHolder](-view-holder/index.md)[onCreateViewHolder](on-create-view-holder.md)([ViewGroup](https://developer.android.com/reference/kotlin/android/view/ViewGroup.html)viewGroup, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)viewType) |
| [setItems](set-items.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setItems](set-items.md)([List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;Berry&gt;items) |
| [setTypes](set-types.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setTypes](set-types.md)([List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[String](https://developer.android.com/reference/kotlin/java/lang/String.html)&gt;types) |

## Properties

| Name | Summary |
|---|---|
| [items](index.md#283116548%2FProperties%2F-1637274268) | [android]<br>private [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;Berry&gt;[items](index.md#283116548%2FProperties%2F-1637274268) |
| [types](index.md#1968022891%2FProperties%2F-1637274268) | [android]<br>private [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[String](https://developer.android.com/reference/kotlin/java/lang/String.html)&gt;[types](index.md#1968022891%2FProperties%2F-1637274268) |
