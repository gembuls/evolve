//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.pokemondetail](../index.md)/[BerryAdapter](index.md)/[getTypes](get-types.md)

# getTypes

[android]\

public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[String](https://developer.android.com/reference/kotlin/java/lang/String.html)&gt;[getTypes](get-types.md)()
