//[evolve_shared](../../index.md)/[example.pokemon.evolve.shared.app.pokemondetail](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [BerryAdapter](-berry-adapter/index.md) | [android]<br>public final class [BerryAdapter](-berry-adapter/index.md) extends [RecyclerView.Adapter](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.Adapter.html)&lt;[BerryAdapter.ViewHolder](-berry-adapter/-view-holder/index.md)&gt; |
| [PokemonDetailFragment](-pokemon-detail-fragment/index.md) | [android]<br>public final class [PokemonDetailFragment](-pokemon-detail-fragment/index.md) extends BaseFragment&lt;&lt;Error class: unknown class&gt;&gt; |
