//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.pokemondetail](../index.md)/[PokemonDetailFragment](index.md)/[onCreate](on-create.md)

# onCreate

[android]\

public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onCreate](on-create.md)([Bundle](https://developer.android.com/reference/kotlin/android/os/Bundle.html)savedInstanceState)
