//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve](../index.md)/[EvolveRepositoryImpl](index.md)/[getPokemonsNetwork](get-pokemons-network.md)

# getPokemonsNetwork

[common]\

public [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;[getPokemonsNetwork](get-pokemons-network.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)offset, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)limit)
