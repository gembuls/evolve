//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve](../index.md)/[EvolveRepositoryImpl](index.md)/[setPokemonsLocal](set-pokemons-local.md)

# setPokemonsLocal

[common]\

public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setPokemonsLocal](set-pokemons-local.md)([List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;pokemons)
