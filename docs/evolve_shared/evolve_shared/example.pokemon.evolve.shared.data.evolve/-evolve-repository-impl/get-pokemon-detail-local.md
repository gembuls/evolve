//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve](../index.md)/[EvolveRepositoryImpl](index.md)/[getPokemonDetailLocal](get-pokemon-detail-local.md)

# getPokemonDetailLocal

[common]\

public [Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)[getPokemonDetailLocal](get-pokemon-detail-local.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)name)
