//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[RealmListPokemonTypeSerializer](index.md)

# RealmListPokemonTypeSerializer

[common]\
public final class [RealmListPokemonTypeSerializer](index.md) implements KSerializer&lt;RealmList&lt;[PokemonType](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-type/index.md)&gt;&gt;

## Constructors

| | |
|---|---|
| [RealmListPokemonTypeSerializer](-realm-list-pokemon-type-serializer.md) | [common]<br>public [RealmListPokemonTypeSerializer](index.md)[RealmListPokemonTypeSerializer](-realm-list-pokemon-type-serializer.md)(KSerializer&lt;[PokemonType](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-type/index.md)&gt;dataSerializer) |

## Functions

| Name | Summary |
|---|---|
| [deserialize](deserialize.md) | [common]<br>public RealmList&lt;[PokemonType](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-type/index.md)&gt;[deserialize](deserialize.md)(Decoderdecoder) |
| [getDataSerializer](get-data-serializer.md) | [common]<br>public final KSerializer&lt;[PokemonType](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-type/index.md)&gt;[getDataSerializer](get-data-serializer.md)() |
| [getDescriptor](get-descriptor.md) | [common]<br>public SerialDescriptor[getDescriptor](get-descriptor.md)() |
| [serialize](serialize.md) | [common]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[serialize](serialize.md)(Encoderencoder, RealmList&lt;[PokemonType](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-type/index.md)&gt;obj) |

## Properties

| Name | Summary |
|---|---|
| [dataSerializer](index.md#-787048461%2FProperties%2F442279350) | [common]<br>private final KSerializer&lt;[PokemonType](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-type/index.md)&gt;[dataSerializer](index.md#-787048461%2FProperties%2F442279350) |
| [descriptor](index.md#1863716288%2FProperties%2F442279350) | [common]<br>private final SerialDescriptor[descriptor](index.md#1863716288%2FProperties%2F442279350) |
