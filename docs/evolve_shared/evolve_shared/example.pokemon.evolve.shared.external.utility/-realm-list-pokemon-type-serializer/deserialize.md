//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[RealmListPokemonTypeSerializer](index.md)/[deserialize](deserialize.md)

# deserialize

[common]\

public RealmList&lt;[PokemonType](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-type/index.md)&gt;[deserialize](deserialize.md)(Decoderdecoder)
