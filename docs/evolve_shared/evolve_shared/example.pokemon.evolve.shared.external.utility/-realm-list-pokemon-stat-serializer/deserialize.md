//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[RealmListPokemonStatSerializer](index.md)/[deserialize](deserialize.md)

# deserialize

[common]\

public RealmList&lt;[PokemonStat](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-stat/index.md)&gt;[deserialize](deserialize.md)(Decoderdecoder)
