//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[RealmListPokemonStatSerializer](index.md)

# RealmListPokemonStatSerializer

[common]\
public final class [RealmListPokemonStatSerializer](index.md) implements KSerializer&lt;RealmList&lt;[PokemonStat](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-stat/index.md)&gt;&gt;

## Constructors

| | |
|---|---|
| [RealmListPokemonStatSerializer](-realm-list-pokemon-stat-serializer.md) | [common]<br>public [RealmListPokemonStatSerializer](index.md)[RealmListPokemonStatSerializer](-realm-list-pokemon-stat-serializer.md)(KSerializer&lt;[PokemonStat](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-stat/index.md)&gt;dataSerializer) |

## Functions

| Name | Summary |
|---|---|
| [deserialize](deserialize.md) | [common]<br>public RealmList&lt;[PokemonStat](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-stat/index.md)&gt;[deserialize](deserialize.md)(Decoderdecoder) |
| [getDataSerializer](get-data-serializer.md) | [common]<br>public final KSerializer&lt;[PokemonStat](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-stat/index.md)&gt;[getDataSerializer](get-data-serializer.md)() |
| [getDescriptor](get-descriptor.md) | [common]<br>public SerialDescriptor[getDescriptor](get-descriptor.md)() |
| [serialize](serialize.md) | [common]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[serialize](serialize.md)(Encoderencoder, RealmList&lt;[PokemonStat](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-stat/index.md)&gt;obj) |

## Properties

| Name | Summary |
|---|---|
| [dataSerializer](index.md#978015373%2FProperties%2F442279350) | [common]<br>private final KSerializer&lt;[PokemonStat](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-stat/index.md)&gt;[dataSerializer](index.md#978015373%2FProperties%2F442279350) |
| [descriptor](index.md#-16439462%2FProperties%2F442279350) | [common]<br>private final SerialDescriptor[descriptor](index.md#-16439462%2FProperties%2F442279350) |
