//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[RealmListPokemonEvolutionSerializer](index.md)/[serialize](serialize.md)

# serialize

[common]\

public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[serialize](serialize.md)(Encoderencoder, RealmList&lt;[PokemonEvolution](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)&gt;obj)
