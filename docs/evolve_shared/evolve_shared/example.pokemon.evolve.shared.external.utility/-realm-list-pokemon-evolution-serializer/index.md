//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[RealmListPokemonEvolutionSerializer](index.md)

# RealmListPokemonEvolutionSerializer

[common]\
public final class [RealmListPokemonEvolutionSerializer](index.md) implements KSerializer&lt;RealmList&lt;[PokemonEvolution](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)&gt;&gt;

## Constructors

| | |
|---|---|
| [RealmListPokemonEvolutionSerializer](-realm-list-pokemon-evolution-serializer.md) | [common]<br>public [RealmListPokemonEvolutionSerializer](index.md)[RealmListPokemonEvolutionSerializer](-realm-list-pokemon-evolution-serializer.md)(KSerializer&lt;[PokemonEvolution](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)&gt;dataSerializer) |

## Functions

| Name | Summary |
|---|---|
| [deserialize](deserialize.md) | [common]<br>public RealmList&lt;[PokemonEvolution](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)&gt;[deserialize](deserialize.md)(Decoderdecoder) |
| [getDataSerializer](get-data-serializer.md) | [common]<br>public final KSerializer&lt;[PokemonEvolution](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)&gt;[getDataSerializer](get-data-serializer.md)() |
| [getDescriptor](get-descriptor.md) | [common]<br>public SerialDescriptor[getDescriptor](get-descriptor.md)() |
| [serialize](serialize.md) | [common]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[serialize](serialize.md)(Encoderencoder, RealmList&lt;[PokemonEvolution](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)&gt;obj) |

## Properties

| Name | Summary |
|---|---|
| [dataSerializer](index.md#-89581910%2FProperties%2F442279350) | [common]<br>private final KSerializer&lt;[PokemonEvolution](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)&gt;[dataSerializer](index.md#-89581910%2FProperties%2F442279350) |
| [descriptor](index.md#2032828407%2FProperties%2F442279350) | [common]<br>private final SerialDescriptor[descriptor](index.md#2032828407%2FProperties%2F442279350) |
