//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[RealmListPokemonEvolutionSerializer](index.md)/[deserialize](deserialize.md)

# deserialize

[common]\

public RealmList&lt;[PokemonEvolution](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)&gt;[deserialize](deserialize.md)(Decoderdecoder)
