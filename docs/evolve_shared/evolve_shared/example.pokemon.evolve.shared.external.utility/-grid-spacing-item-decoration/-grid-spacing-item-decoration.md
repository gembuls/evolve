//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[GridSpacingItemDecoration](index.md)/[GridSpacingItemDecoration](-grid-spacing-item-decoration.md)

# GridSpacingItemDecoration

[android]\

public [GridSpacingItemDecoration](index.md)[GridSpacingItemDecoration](-grid-spacing-item-decoration.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)spanCount, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)spacing, [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)includeEdge)
