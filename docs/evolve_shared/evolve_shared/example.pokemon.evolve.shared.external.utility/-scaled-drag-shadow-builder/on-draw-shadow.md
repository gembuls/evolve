//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[ScaledDragShadowBuilder](index.md)/[onDrawShadow](on-draw-shadow.md)

# onDrawShadow

[android]\

public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onDrawShadow](on-draw-shadow.md)([Canvas](https://developer.android.com/reference/kotlin/android/graphics/Canvas.html)canvas)
