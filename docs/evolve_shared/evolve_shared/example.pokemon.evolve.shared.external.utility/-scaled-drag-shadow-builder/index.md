//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.external.utility](../index.md)/[ScaledDragShadowBuilder](index.md)

# ScaledDragShadowBuilder

[android]\
public final class [ScaledDragShadowBuilder](index.md) extends [View.DragShadowBuilder](https://developer.android.com/reference/kotlin/android/view/View.DragShadowBuilder.html)

## Constructors

| | |
|---|---|
| [ScaledDragShadowBuilder](-scaled-drag-shadow-builder.md) | [android]<br>public [ScaledDragShadowBuilder](index.md)[ScaledDragShadowBuilder](-scaled-drag-shadow-builder.md)([View](https://developer.android.com/reference/kotlin/android/view/View.html)view, [Float](https://developer.android.com/reference/kotlin/java/lang/Float.html)scale) |

## Functions

| Name | Summary |
|---|---|
| [onDrawShadow](on-draw-shadow.md) | [android]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onDrawShadow](on-draw-shadow.md)([Canvas](https://developer.android.com/reference/kotlin/android/graphics/Canvas.html)canvas) |
| [onProvideShadowMetrics](on-provide-shadow-metrics.md) | [android]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onProvideShadowMetrics](on-provide-shadow-metrics.md)([Point](https://developer.android.com/reference/kotlin/android/graphics/Point.html)outShadowSize, [Point](https://developer.android.com/reference/kotlin/android/graphics/Point.html)outShadowTouchPoint) |
