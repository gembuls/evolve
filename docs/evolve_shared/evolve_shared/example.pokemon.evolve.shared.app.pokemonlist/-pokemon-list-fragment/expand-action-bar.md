//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.pokemonlist](../index.md)/[PokemonListFragment](index.md)/[expandActionBar](expand-action-bar.md)

# expandActionBar

[android]\

public [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[expandActionBar](expand-action-bar.md)()
