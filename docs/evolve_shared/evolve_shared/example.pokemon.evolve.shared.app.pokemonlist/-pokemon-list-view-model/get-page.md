//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.pokemonlist](../index.md)/[PokemonListViewModel](index.md)/[getPage](get-page.md)

# getPage

[common]\

public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getPage](get-page.md)()
