//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.pokemonlist](../index.md)/[PokemonListViewModel](index.md)/[setAllPokemons](set-all-pokemons.md)

# setAllPokemons

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setAllPokemons](set-all-pokemons.md)(MutableStateFlow&lt;[List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;&gt;allPokemons)
