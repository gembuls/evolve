//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.pokemonlist](../index.md)/[PokemonListViewModel](index.md)/[isChosen](is-chosen.md)

# isChosen

[common]\

public final [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[isChosen](is-chosen.md)()
