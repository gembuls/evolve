//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.pokemonlist](../index.md)/[PokemonListViewModel](index.md)/[getSearch](get-search.md)

# getSearch

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getSearch](get-search.md)()
