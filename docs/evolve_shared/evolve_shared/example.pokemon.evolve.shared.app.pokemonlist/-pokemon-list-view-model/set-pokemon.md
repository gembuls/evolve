//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.pokemonlist](../index.md)/[PokemonListViewModel](index.md)/[setPokemon](set-pokemon.md)

# setPokemon

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setPokemon](set-pokemon.md)(MutableStateFlow&lt;[PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)&gt;pokemon)
