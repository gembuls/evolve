//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.pokemonlist](../index.md)/[PokemonListViewModel](index.md)/[setPage](set-page.md)

# setPage

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setPage](set-page.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)page)
