//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonStat](index.md)/[getBase_stat](get-base_stat.md)

# getBase_stat

[common]\

public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getBase_stat](get-base_stat.md)()
