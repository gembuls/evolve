//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonEvolution](index.md)/[getEvolvesTo](get-evolves-to.md)

# getEvolvesTo

[common]\

public final RealmList&lt;[PokemonEvolution](index.md)&gt;[getEvolvesTo](get-evolves-to.md)()
