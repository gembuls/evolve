//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonEvolution](index.md)/[setEvolvesTo](set-evolves-to.md)

# setEvolvesTo

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setEvolvesTo](set-evolves-to.md)(@Serializable(with = [RealmListPokemonEvolutionSerializer.class](../../example.pokemon.evolve.shared.external.utility/-realm-list-pokemon-evolution-serializer/index.md))RealmList&lt;[PokemonEvolution](index.md)&gt;evolvesTo)
