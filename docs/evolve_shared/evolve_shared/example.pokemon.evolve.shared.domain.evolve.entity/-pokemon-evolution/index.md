//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonEvolution](index.md)

# PokemonEvolution

[common]\
@Serializable()

public class [PokemonEvolution](index.md) implements RealmObject

## Constructors

| | |
|---|---|
| [PokemonEvolution](-pokemon-evolution.md) | [common]<br>public [PokemonEvolution](index.md)[PokemonEvolution](-pokemon-evolution.md)() |

## Functions

| Name | Summary |
|---|---|
| [clone](clone.md) | [common]<br>public final [PokemonEvolution](index.md)[clone](clone.md)() |
| [getEvolvesTo](get-evolves-to.md) | [common]<br>public final RealmList&lt;[PokemonEvolution](index.md)&gt;[getEvolvesTo](get-evolves-to.md)() |
| [getId](get-id.md) | [common]<br>public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getId](get-id.md)() |
| [getSpecies](get-species.md) | [common]<br>public final [PokemonUrl](../-pokemon-url/index.md)[getSpecies](get-species.md)() |
| [setEvolvesTo](set-evolves-to.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setEvolvesTo](set-evolves-to.md)(@Serializable(with = [RealmListPokemonEvolutionSerializer.class](../../example.pokemon.evolve.shared.external.utility/-realm-list-pokemon-evolution-serializer/index.md))RealmList&lt;[PokemonEvolution](index.md)&gt;evolvesTo) |
| [setId](set-id.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setId](set-id.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)id) |
| [setSpecies](set-species.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setSpecies](set-species.md)([PokemonUrl](../-pokemon-url/index.md)species) |

## Properties

| Name | Summary |
|---|---|
| [evolvesTo](index.md#1175079493%2FProperties%2F442279350) | [common]<br>@Serializable(with = [RealmListPokemonEvolutionSerializer.class](../../example.pokemon.evolve.shared.external.utility/-realm-list-pokemon-evolution-serializer/index.md))<br>private RealmList&lt;[PokemonEvolution](index.md)&gt;[evolvesTo](index.md#1175079493%2FProperties%2F442279350) |
| [id](index.md#-1019592695%2FProperties%2F442279350) | [common]<br>private [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[id](index.md#-1019592695%2FProperties%2F442279350) |
| [species](index.md#93308250%2FProperties%2F442279350) | [common]<br>private [PokemonUrl](../-pokemon-url/index.md)[species](index.md#93308250%2FProperties%2F442279350) |
