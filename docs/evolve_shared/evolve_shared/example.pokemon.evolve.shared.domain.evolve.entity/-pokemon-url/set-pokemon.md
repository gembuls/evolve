//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonUrl](index.md)/[setPokemon](set-pokemon.md)

# setPokemon

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setPokemon](set-pokemon.md)([Pokemon](../-pokemon/index.md)pokemon)
