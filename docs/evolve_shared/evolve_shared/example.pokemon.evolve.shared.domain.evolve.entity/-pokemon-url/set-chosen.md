//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonUrl](index.md)/[setChosen](set-chosen.md)

# setChosen

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setChosen](set-chosen.md)([Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)isChosen)
