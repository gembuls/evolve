//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonUrl](index.md)/[getOrder](get-order.md)

# getOrder

[common]\

public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getOrder](get-order.md)()
