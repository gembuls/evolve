//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Pokemon](index.md)/[setSpecies](set-species.md)

# setSpecies

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setSpecies](set-species.md)([PokemonUrl](../-pokemon-url/index.md)species)
