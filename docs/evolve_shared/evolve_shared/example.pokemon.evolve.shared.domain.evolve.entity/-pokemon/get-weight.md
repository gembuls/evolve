//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Pokemon](index.md)/[getWeight](get-weight.md)

# getWeight

[common]\

public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getWeight](get-weight.md)()
