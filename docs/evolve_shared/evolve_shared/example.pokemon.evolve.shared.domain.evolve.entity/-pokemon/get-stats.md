//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Pokemon](index.md)/[getStats](get-stats.md)

# getStats

[common]\

public final RealmList&lt;[PokemonStat](../-pokemon-stat/index.md)&gt;[getStats](get-stats.md)()
