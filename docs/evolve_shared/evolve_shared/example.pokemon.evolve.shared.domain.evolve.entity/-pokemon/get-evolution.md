//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Pokemon](index.md)/[getEvolution](get-evolution.md)

# getEvolution

[common]\

public final [PokemonEvolution](../-pokemon-evolution/index.md)[getEvolution](get-evolution.md)()
