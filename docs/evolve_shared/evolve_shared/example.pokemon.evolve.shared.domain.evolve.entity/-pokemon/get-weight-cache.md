//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Pokemon](index.md)/[getWeightCache](get-weight-cache.md)

# getWeightCache

[common]\

public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getWeightCache](get-weight-cache.md)()
