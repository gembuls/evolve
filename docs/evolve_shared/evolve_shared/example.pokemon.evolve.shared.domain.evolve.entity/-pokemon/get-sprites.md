//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Pokemon](index.md)/[getSprites](get-sprites.md)

# getSprites

[common]\

public final [PokemonSpritesResp](../../example.pokemon.evolve.shared.data.evolve.network.response/-pokemon-sprites-resp/index.md)[getSprites](get-sprites.md)()
