//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Pokemon](index.md)/[setEvolution](set-evolution.md)

# setEvolution

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setEvolution](set-evolution.md)([PokemonEvolution](../-pokemon-evolution/index.md)evolution)
