//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Pokemon](index.md)/[setSprites](set-sprites.md)

# setSprites

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setSprites](set-sprites.md)([PokemonSpritesResp](../../example.pokemon.evolve.shared.data.evolve.network.response/-pokemon-sprites-resp/index.md)sprites)
