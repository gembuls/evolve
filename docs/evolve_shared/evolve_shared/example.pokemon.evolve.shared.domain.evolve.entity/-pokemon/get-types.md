//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Pokemon](index.md)/[getTypes](get-types.md)

# getTypes

[common]\

public final RealmList&lt;[PokemonType](../-pokemon-type/index.md)&gt;[getTypes](get-types.md)()
