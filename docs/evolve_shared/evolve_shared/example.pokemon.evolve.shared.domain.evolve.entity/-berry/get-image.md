//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Berry](index.md)/[getImage](get-image.md)

# getImage

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getImage](get-image.md)()
