//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Berry](index.md)/[setNaturalGiftType](set-natural-gift-type.md)

# setNaturalGiftType

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setNaturalGiftType](set-natural-gift-type.md)([PokemonUrl](../-pokemon-url/index.md)naturalGiftType)
