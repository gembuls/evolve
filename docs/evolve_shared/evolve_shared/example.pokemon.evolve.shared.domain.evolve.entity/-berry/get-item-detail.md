//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Berry](index.md)/[getItemDetail](get-item-detail.md)

# getItemDetail

[common]\

public final [PokemonItemResp](../../example.pokemon.evolve.shared.data.evolve.network.response/-pokemon-item-resp/index.md)[getItemDetail](get-item-detail.md)()
