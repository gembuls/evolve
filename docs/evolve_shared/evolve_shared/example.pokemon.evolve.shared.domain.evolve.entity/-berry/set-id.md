//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Berry](index.md)/[setId](set-id.md)

# setId

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setId](set-id.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)id)
