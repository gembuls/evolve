//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Berry](index.md)/[setImage](set-image.md)

# setImage

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setImage](set-image.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)image)
