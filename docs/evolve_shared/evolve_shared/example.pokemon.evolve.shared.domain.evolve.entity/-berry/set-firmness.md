//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Berry](index.md)/[setFirmness](set-firmness.md)

# setFirmness

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setFirmness](set-firmness.md)([PokemonUrl](../-pokemon-url/index.md)firmness)
