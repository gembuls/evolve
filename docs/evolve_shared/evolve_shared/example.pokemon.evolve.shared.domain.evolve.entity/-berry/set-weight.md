//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Berry](index.md)/[setWeight](set-weight.md)

# setWeight

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setWeight](set-weight.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)weight)
