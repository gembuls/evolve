//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[Berry](index.md)

# Berry

[common]\
@Serializable()

public class [Berry](index.md) implements RealmObject

## Constructors

| | |
|---|---|
| [Berry](-berry.md) | [common]<br>public [Berry](index.md)[Berry](-berry.md)() |

## Functions

| Name | Summary |
|---|---|
| [clone](clone.md) | [common]<br>public final [Berry](index.md)[clone](clone.md)() |
| [getFirmness](get-firmness.md) | [common]<br>public final [PokemonUrl](../-pokemon-url/index.md)[getFirmness](get-firmness.md)() |
| [getId](get-id.md) | [common]<br>public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getId](get-id.md)() |
| [getImage](get-image.md) | [common]<br>public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getImage](get-image.md)() |
| [getItem](get-item.md) | [common]<br>public final [PokemonUrl](../-pokemon-url/index.md)[getItem](get-item.md)() |
| [getItemDetail](get-item-detail.md) | [common]<br>public final [PokemonItemResp](../../example.pokemon.evolve.shared.data.evolve.network.response/-pokemon-item-resp/index.md)[getItemDetail](get-item-detail.md)() |
| [getName](get-name.md) | [common]<br>public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getName](get-name.md)() |
| [getNaturalGiftType](get-natural-gift-type.md) | [common]<br>public final [PokemonUrl](../-pokemon-url/index.md)[getNaturalGiftType](get-natural-gift-type.md)() |
| [getWeight](get-weight.md) | [common]<br>public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getWeight](get-weight.md)() |
| [setFirmness](set-firmness.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setFirmness](set-firmness.md)([PokemonUrl](../-pokemon-url/index.md)firmness) |
| [setId](set-id.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setId](set-id.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)id) |
| [setImage](set-image.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setImage](set-image.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)image) |
| [setItem](set-item.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setItem](set-item.md)([PokemonUrl](../-pokemon-url/index.md)item) |
| [setItemDetail](set-item-detail.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setItemDetail](set-item-detail.md)([PokemonItemResp](../../example.pokemon.evolve.shared.data.evolve.network.response/-pokemon-item-resp/index.md)itemDetail) |
| [setName](set-name.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setName](set-name.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)name) |
| [setNaturalGiftType](set-natural-gift-type.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setNaturalGiftType](set-natural-gift-type.md)([PokemonUrl](../-pokemon-url/index.md)naturalGiftType) |
| [setWeight](set-weight.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setWeight](set-weight.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)weight) |

## Properties

| Name | Summary |
|---|---|
| [firmness](index.md#-1699395679%2FProperties%2F442279350) | [common]<br>private [PokemonUrl](../-pokemon-url/index.md)[firmness](index.md#-1699395679%2FProperties%2F442279350) |
| [id](index.md#959669659%2FProperties%2F442279350) | [common]<br>private [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[id](index.md#959669659%2FProperties%2F442279350) |
| [image](index.md#1578862985%2FProperties%2F442279350) | [common]<br>private [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[image](index.md#1578862985%2FProperties%2F442279350) |
| [item](index.md#1155456099%2FProperties%2F442279350) | [common]<br>private [PokemonUrl](../-pokemon-url/index.md)[item](index.md#1155456099%2FProperties%2F442279350) |
| [itemDetail](index.md#1630172306%2FProperties%2F442279350) | [common]<br>private [PokemonItemResp](../../example.pokemon.evolve.shared.data.evolve.network.response/-pokemon-item-resp/index.md)[itemDetail](index.md#1630172306%2FProperties%2F442279350) |
| [name](index.md#-274156565%2FProperties%2F442279350) | [common]<br>private [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[name](index.md#-274156565%2FProperties%2F442279350) |
| [naturalGiftType](index.md#837535057%2FProperties%2F442279350) | [common]<br>private [PokemonUrl](../-pokemon-url/index.md)[naturalGiftType](index.md#837535057%2FProperties%2F442279350) |
| [weight](index.md#-632156578%2FProperties%2F442279350) | [common]<br>private [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[weight](index.md#-632156578%2FProperties%2F442279350) |
