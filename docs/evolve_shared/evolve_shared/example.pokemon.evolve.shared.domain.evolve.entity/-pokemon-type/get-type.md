//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](../index.md)/[PokemonType](index.md)/[getType](get-type.md)

# getType

[common]\

public final [PokemonUrl](../-pokemon-url/index.md)[getType](get-type.md)()
