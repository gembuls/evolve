//[evolve_shared](../../index.md)/[example.pokemon.evolve.shared.domain.evolve.entity](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [Berry](-berry/index.md) | [common]<br>@Serializable()<br>public class [Berry](-berry/index.md) implements RealmObject |
| [Pokemon](-pokemon/index.md) | [common]<br>@Serializable()<br>public class [Pokemon](-pokemon/index.md) implements RealmObject |
| [PokemonEvolution](-pokemon-evolution/index.md) | [common]<br>@Serializable()<br>public class [PokemonEvolution](-pokemon-evolution/index.md) implements RealmObject |
| [PokemonStat](-pokemon-stat/index.md) | [common]<br>@Serializable()<br>public class [PokemonStat](-pokemon-stat/index.md) implements RealmObject |
| [PokemonType](-pokemon-type/index.md) | [common]<br>@Serializable()<br>public class [PokemonType](-pokemon-type/index.md) implements RealmObject |
| [PokemonUrl](-pokemon-url/index.md) | [common]<br>@Serializable()<br>public class [PokemonUrl](-pokemon-url/index.md) implements RealmObject |
