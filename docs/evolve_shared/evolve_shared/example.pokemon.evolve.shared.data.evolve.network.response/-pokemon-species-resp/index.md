//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonSpeciesResp](index.md)

# PokemonSpeciesResp

[common]\
@Serializable()

public final class [PokemonSpeciesResp](index.md)

## Constructors

| | |
|---|---|
| [PokemonSpeciesResp](-pokemon-species-resp.md) | [common]<br>public [PokemonSpeciesResp](index.md)[PokemonSpeciesResp](-pokemon-species-resp.md)([PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)evolutionChain, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)name) |

## Functions

| Name | Summary |
|---|---|
| [getEvolutionChain](get-evolution-chain.md) | [common]<br>public final [PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)[getEvolutionChain](get-evolution-chain.md)() |
| [getName](get-name.md) | [common]<br>public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getName](get-name.md)() |
| [setEvolutionChain](set-evolution-chain.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setEvolutionChain](set-evolution-chain.md)([PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)evolutionChain) |
| [setName](set-name.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setName](set-name.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)name) |

## Properties

| Name | Summary |
|---|---|
| [evolutionChain](index.md#-763085525%2FProperties%2F442279350) | [common]<br>private [PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)[evolutionChain](index.md#-763085525%2FProperties%2F442279350) |
| [name](index.md#152076102%2FProperties%2F442279350) | [common]<br>private [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[name](index.md#152076102%2FProperties%2F442279350) |
