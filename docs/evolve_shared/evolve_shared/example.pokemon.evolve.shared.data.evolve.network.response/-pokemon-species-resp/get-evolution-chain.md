//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonSpeciesResp](index.md)/[getEvolutionChain](get-evolution-chain.md)

# getEvolutionChain

[common]\

public final [PokemonUrl](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-url/index.md)[getEvolutionChain](get-evolution-chain.md)()
