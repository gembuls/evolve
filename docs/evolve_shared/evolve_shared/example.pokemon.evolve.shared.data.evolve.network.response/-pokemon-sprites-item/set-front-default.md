//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonSpritesItem](index.md)/[setFrontDefault](set-front-default.md)

# setFrontDefault

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setFrontDefault](set-front-default.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)frontDefault)
