//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonSpritesResp](index.md)

# PokemonSpritesResp

[common]\
@Serializable()

public final class [PokemonSpritesResp](index.md)

## Constructors

| | |
|---|---|
| [PokemonSpritesResp](-pokemon-sprites-resp.md) | [common]<br>public [PokemonSpritesResp](index.md)[PokemonSpritesResp](-pokemon-sprites-resp.md)([PokemonSpritesOther](../-pokemon-sprites-other/index.md)other) |

## Functions

| Name | Summary |
|---|---|
| [getOther](get-other.md) | [common]<br>public final [PokemonSpritesOther](../-pokemon-sprites-other/index.md)[getOther](get-other.md)() |
| [setOther](set-other.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setOther](set-other.md)([PokemonSpritesOther](../-pokemon-sprites-other/index.md)other) |

## Properties

| Name | Summary |
|---|---|
| [other](index.md#589200167%2FProperties%2F442279350) | [common]<br>private [PokemonSpritesOther](../-pokemon-sprites-other/index.md)[other](index.md#589200167%2FProperties%2F442279350) |
