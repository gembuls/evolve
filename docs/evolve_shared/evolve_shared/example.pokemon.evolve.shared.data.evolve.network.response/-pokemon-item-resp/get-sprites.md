//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonItemResp](index.md)/[getSprites](get-sprites.md)

# getSprites

[common]\

public final [PokemonSpritesItem](../-pokemon-sprites-item/index.md)[getSprites](get-sprites.md)()
