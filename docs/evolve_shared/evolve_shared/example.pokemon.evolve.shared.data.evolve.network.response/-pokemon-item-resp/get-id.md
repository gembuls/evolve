//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonItemResp](index.md)/[getId](get-id.md)

# getId

[common]\

public final [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getId](get-id.md)()
