//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonItemResp](index.md)/[getName](get-name.md)

# getName

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[getName](get-name.md)()
