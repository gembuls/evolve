//[evolve_shared](../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [BerryListResp](-berry-list-resp/index.md) | [common]<br>@Serializable()<br>public final class [BerryListResp](-berry-list-resp/index.md) |
| [PokemonEvolutionResp](-pokemon-evolution-resp/index.md) | [common]<br>@Serializable()<br>public final class [PokemonEvolutionResp](-pokemon-evolution-resp/index.md) |
| [PokemonItemResp](-pokemon-item-resp/index.md) | [common]<br>@Serializable()<br>public final class [PokemonItemResp](-pokemon-item-resp/index.md) |
| [PokemonListResp](-pokemon-list-resp/index.md) | [common]<br>@Serializable()<br>public final class [PokemonListResp](-pokemon-list-resp/index.md) |
| [PokemonSpeciesResp](-pokemon-species-resp/index.md) | [common]<br>@Serializable()<br>public final class [PokemonSpeciesResp](-pokemon-species-resp/index.md) |
| [PokemonSpritesItem](-pokemon-sprites-item/index.md) | [common]<br>@Serializable()<br>public final class [PokemonSpritesItem](-pokemon-sprites-item/index.md) |
| [PokemonSpritesOther](-pokemon-sprites-other/index.md) | [common]<br>@Serializable()<br>public final class [PokemonSpritesOther](-pokemon-sprites-other/index.md) |
| [PokemonSpritesResp](-pokemon-sprites-resp/index.md) | [common]<br>@Serializable()<br>public final class [PokemonSpritesResp](-pokemon-sprites-resp/index.md) |
