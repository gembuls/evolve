//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonSpritesOther](index.md)/[getOfficialArtwork](get-official-artwork.md)

# getOfficialArtwork

[common]\

public final [PokemonSpritesItem](../-pokemon-sprites-item/index.md)[getOfficialArtwork](get-official-artwork.md)()
