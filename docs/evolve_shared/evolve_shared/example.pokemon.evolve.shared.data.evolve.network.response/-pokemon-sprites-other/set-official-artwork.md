//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonSpritesOther](index.md)/[setOfficialArtwork](set-official-artwork.md)

# setOfficialArtwork

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setOfficialArtwork](set-official-artwork.md)([PokemonSpritesItem](../-pokemon-sprites-item/index.md)officialArtwork)
