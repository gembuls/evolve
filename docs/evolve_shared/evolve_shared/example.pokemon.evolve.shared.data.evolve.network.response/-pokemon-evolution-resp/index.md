//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.data.evolve.network.response](../index.md)/[PokemonEvolutionResp](index.md)

# PokemonEvolutionResp

[common]\
@Serializable()

public final class [PokemonEvolutionResp](index.md)

## Constructors

| | |
|---|---|
| [PokemonEvolutionResp](-pokemon-evolution-resp.md) | [common]<br>public [PokemonEvolutionResp](index.md)[PokemonEvolutionResp](-pokemon-evolution-resp.md)([PokemonEvolution](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)chain) |

## Functions

| Name | Summary |
|---|---|
| [getChain](get-chain.md) | [common]<br>public final [PokemonEvolution](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)[getChain](get-chain.md)() |
| [setChain](set-chain.md) | [common]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setChain](set-chain.md)([PokemonEvolution](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)chain) |

## Properties

| Name | Summary |
|---|---|
| [chain](index.md#218283529%2FProperties%2F442279350) | [common]<br>private [PokemonEvolution](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)[chain](index.md#218283529%2FProperties%2F442279350) |
