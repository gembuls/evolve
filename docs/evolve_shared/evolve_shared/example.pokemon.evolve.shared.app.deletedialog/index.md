//[evolve_shared](../../index.md)/[example.pokemon.evolve.shared.app.deletedialog](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [DeleteDialogFragment](-delete-dialog-fragment/index.md) | [android]<br>public final class [DeleteDialogFragment](-delete-dialog-fragment/index.md) extends BaseDialogFragment&lt;&lt;Error class: unknown class&gt;&gt; |
