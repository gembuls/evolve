//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonAdapter](index.md)/[setItemsCache](set-items-cache.md)

# setItemsCache

[android]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setItemsCache](set-items-cache.md)([List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;PokemonUrl&gt;itemsCache)
