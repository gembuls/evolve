//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonAdapter](index.md)/[getItemsCache](get-items-cache.md)

# getItemsCache

[android]\

public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;PokemonUrl&gt;[getItemsCache](get-items-cache.md)()
