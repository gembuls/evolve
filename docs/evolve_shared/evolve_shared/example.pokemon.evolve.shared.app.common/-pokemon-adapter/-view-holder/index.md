//[evolve_shared](../../../../index.md)/[example.pokemon.evolve.shared.app.common](../../index.md)/[PokemonAdapter](../index.md)/[ViewHolder](index.md)

# ViewHolder

[android]\
public final class [ViewHolder](index.md) extends [RecyclerView.ViewHolder](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.ViewHolder.html)

## Constructors

| | |
|---|---|
| [PokemonAdapter.ViewHolder](-pokemon-adapter.-view-holder.md) | [android]<br>public [PokemonAdapter.ViewHolder](index.md)[PokemonAdapter.ViewHolder](-pokemon-adapter.-view-holder.md)(&lt;Error class: unknown class&gt;binding) |

## Functions

| Name | Summary |
|---|---|
| [bind](bind.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[bind](bind.md)(PokemonUrlitem) |
