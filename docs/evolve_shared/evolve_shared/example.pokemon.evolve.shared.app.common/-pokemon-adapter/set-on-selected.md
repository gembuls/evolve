//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonAdapter](index.md)/[setOnSelected](set-on-selected.md)

# setOnSelected

[android]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setOnSelected](set-on-selected.md)(Function2&lt;[View](https://developer.android.com/reference/kotlin/android/view/View.html), PokemonUrl, [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)&gt;onSelected)
