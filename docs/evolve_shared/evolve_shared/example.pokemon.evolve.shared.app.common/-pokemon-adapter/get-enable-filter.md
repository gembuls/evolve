//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonAdapter](index.md)/[getEnableFilter](get-enable-filter.md)

# getEnableFilter

[android]\

public final [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[getEnableFilter](get-enable-filter.md)()
