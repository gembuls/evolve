//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonAdapter](index.md)

# PokemonAdapter

[android]\
public final class [PokemonAdapter](index.md) extends [RecyclerView.Adapter](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.Adapter.html)&lt;[PokemonAdapter.ViewHolder](-view-holder/index.md)&gt;

## Constructors

| | |
|---|---|
| [PokemonAdapter](-pokemon-adapter.md) | [android]<br>public [PokemonAdapter](index.md)[PokemonAdapter](-pokemon-adapter.md)([Double](https://developer.android.com/reference/kotlin/java/lang/Double.html)widthRatio, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)height, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)radius, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)elevation) |

## Types

| Name | Summary |
|---|---|
| [Listener](-listener/index.md) | [android]<br>public final class [Listener](-listener/index.md) extends [RecyclerView.OnScrollListener](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.OnScrollListener.html) |
| [ViewHolder](-view-holder/index.md) | [android]<br>public final class [ViewHolder](-view-holder/index.md) extends [RecyclerView.ViewHolder](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.ViewHolder.html) |

## Functions

| Name | Summary |
|---|---|
| [addItem](add-item.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[addItem](add-item.md)(Function0&lt;[Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)&gt;process) |
| [clear](clear.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[clear](clear.md)() |
| [getEnableFilter](get-enable-filter.md) | [android]<br>public final [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[getEnableFilter](get-enable-filter.md)() |
| [getFetchData](get-fetch-data.md) | [android]<br>public final Function0&lt;[Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)&gt;[getFetchData](get-fetch-data.md)() |
| [getItemCount](get-item-count.md) | [android]<br>public [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)[getItemCount](get-item-count.md)() |
| [getItems](get-items.md) | [android]<br>public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;PokemonUrl&gt;[getItems](get-items.md)() |
| [getItemsCache](get-items-cache.md) | [android]<br>public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;PokemonUrl&gt;[getItemsCache](get-items-cache.md)() |
| [getOnSelected](get-on-selected.md) | [android]<br>public final Function2&lt;[View](https://developer.android.com/reference/kotlin/android/view/View.html), PokemonUrl, [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)&gt;[getOnSelected](get-on-selected.md)() |
| [getSkeleton](get-skeleton.md) | [android]<br>public final PokemonUrl[getSkeleton](get-skeleton.md)() |
| [isLastPage](is-last-page.md) | [android]<br>public final [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[isLastPage](is-last-page.md)() |
| [onBindViewHolder](on-bind-view-holder.md) | [android]<br>public [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[onBindViewHolder](on-bind-view-holder.md)([PokemonAdapter.ViewHolder](-view-holder/index.md)viewHolder, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)position) |
| [onCreateViewHolder](on-create-view-holder.md) | [android]<br>public [PokemonAdapter.ViewHolder](-view-holder/index.md)[onCreateViewHolder](on-create-view-holder.md)([ViewGroup](https://developer.android.com/reference/kotlin/android/view/ViewGroup.html)viewGroup, [Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)viewType) |
| [setEnableFilter](set-enable-filter.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setEnableFilter](set-enable-filter.md)([Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)enableFilter) |
| [setFetchData](set-fetch-data.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setFetchData](set-fetch-data.md)(Function0&lt;[Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)&gt;fetchData) |
| [setItems](set-items.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setItems](set-items.md)([List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;PokemonUrl&gt;items) |
| [setItemsCache](set-items-cache.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setItemsCache](set-items-cache.md)([List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;PokemonUrl&gt;itemsCache) |
| [setLastPage](set-last-page.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setLastPage](set-last-page.md)([Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)isLastPage) |
| [setOnSelected](set-on-selected.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setOnSelected](set-on-selected.md)(Function2&lt;[View](https://developer.android.com/reference/kotlin/android/view/View.html), PokemonUrl, [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)&gt;onSelected) |
| [setSkeleton](set-skeleton.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setSkeleton](set-skeleton.md)(PokemonUrlskeleton) |
| [showSkeletons](show-skeletons.md) | [android]<br>public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[showSkeletons](show-skeletons.md)([Integer](https://developer.android.com/reference/kotlin/java/lang/Integer.html)size) |

## Properties

| Name | Summary |
|---|---|
| [enableFilter](index.md#147057807%2FProperties%2F-1637274268) | [android]<br>private [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[enableFilter](index.md#147057807%2FProperties%2F-1637274268) |
| [fetchData](index.md#-316321140%2FProperties%2F-1637274268) | [android]<br>private Function0&lt;[Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)&gt;[fetchData](index.md#-316321140%2FProperties%2F-1637274268) |
| [isLastPage](is-last-page.md) | [android]<br>private [Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)[isLastPage](is-last-page.md) |
| [items](index.md#-224885424%2FProperties%2F-1637274268) | [android]<br>private [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;PokemonUrl&gt;[items](index.md#-224885424%2FProperties%2F-1637274268) |
| [itemsCache](index.md#793557224%2FProperties%2F-1637274268) | [android]<br>private [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;PokemonUrl&gt;[itemsCache](index.md#793557224%2FProperties%2F-1637274268) |
| [onSelected](index.md#909867504%2FProperties%2F-1637274268) | [android]<br>private Function2&lt;[View](https://developer.android.com/reference/kotlin/android/view/View.html), PokemonUrl, [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)&gt;[onSelected](index.md#909867504%2FProperties%2F-1637274268) |
| [skeleton](index.md#563164637%2FProperties%2F-1637274268) | [android]<br>private PokemonUrl[skeleton](index.md#563164637%2FProperties%2F-1637274268) |
