//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonAdapter](index.md)/[setEnableFilter](set-enable-filter.md)

# setEnableFilter

[android]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setEnableFilter](set-enable-filter.md)([Boolean](https://developer.android.com/reference/kotlin/java/lang/Boolean.html)enableFilter)
