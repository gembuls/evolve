//[evolve_shared](../../../../index.md)/[example.pokemon.evolve.shared.app.common](../../index.md)/[PokemonAdapter](../index.md)/[Listener](index.md)/[PokemonAdapter.Listener](-pokemon-adapter.-listener.md)

# PokemonAdapter.Listener

[android]\

public [PokemonAdapter.Listener](index.md)[PokemonAdapter.Listener](-pokemon-adapter.-listener.md)([SwipeRefreshLayout](https://developer.android.com/reference/kotlin/androidx/swiperefreshlayout/widget/SwipeRefreshLayout.html)swipeRefreshLayout)
