//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonStatAdapter](index.md)/[setItems](set-items.md)

# setItems

[android]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setItems](set-items.md)([List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;PokemonStat&gt;items)
