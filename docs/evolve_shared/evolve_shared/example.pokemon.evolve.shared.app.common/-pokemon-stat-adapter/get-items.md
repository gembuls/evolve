//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonStatAdapter](index.md)/[getItems](get-items.md)

# getItems

[android]\

public final [List](https://developer.android.com/reference/kotlin/java/util/List.html)&lt;PokemonStat&gt;[getItems](get-items.md)()
