//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonDetailViewModel](index.md)/[getPokemon](get-pokemon.md)

# getPokemon

[common]\

public final MutableStateFlow&lt;[Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)&gt;[getPokemon](get-pokemon.md)()
