//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonDetailViewModel](index.md)/[checkEvolution](check-evolution.md)

# checkEvolution

[common]\

public final [String](https://developer.android.com/reference/kotlin/java/lang/String.html)[checkEvolution](check-evolution.md)([PokemonEvolution](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon-evolution/index.md)pokemonEvolution, [String](https://developer.android.com/reference/kotlin/java/lang/String.html)name)
