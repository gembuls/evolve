//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonDetailViewModel](index.md)/[getPokemonDetailLocal](get-pokemon-detail-local.md)

# getPokemonDetailLocal

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[getPokemonDetailLocal](get-pokemon-detail-local.md)([String](https://developer.android.com/reference/kotlin/java/lang/String.html)nameArg)
