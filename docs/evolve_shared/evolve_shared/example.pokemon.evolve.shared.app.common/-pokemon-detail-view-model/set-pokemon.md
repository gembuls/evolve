//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonDetailViewModel](index.md)/[setPokemon](set-pokemon.md)

# setPokemon

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setPokemon](set-pokemon.md)(MutableStateFlow&lt;[Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)&gt;pokemon)
