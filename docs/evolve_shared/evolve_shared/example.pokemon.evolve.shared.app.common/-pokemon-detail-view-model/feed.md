//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonDetailViewModel](index.md)/[feed](feed.md)

# feed

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[feed](feed.md)([Berry](../../example.pokemon.evolve.shared.domain.evolve.entity/-berry/index.md)berry)
