//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonDetailViewModel](index.md)/[setNextPokemon](set-next-pokemon.md)

# setNextPokemon

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setNextPokemon](set-next-pokemon.md)(MutableStateFlow&lt;[Pokemon](../../example.pokemon.evolve.shared.domain.evolve.entity/-pokemon/index.md)&gt;nextPokemon)
