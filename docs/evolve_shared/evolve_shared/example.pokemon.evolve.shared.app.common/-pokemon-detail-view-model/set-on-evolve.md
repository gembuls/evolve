//[evolve_shared](../../../index.md)/[example.pokemon.evolve.shared.app.common](../index.md)/[PokemonDetailViewModel](index.md)/[setOnEvolve](set-on-evolve.md)

# setOnEvolve

[common]\

public final [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)[setOnEvolve](set-on-evolve.md)(MutableStateFlow&lt;[String](https://developer.android.com/reference/kotlin/java/lang/String.html)&gt;onEvolve)
