//[evolve_shared](../../index.md)/[example.pokemon.evolve.shared.app.common](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [PokemonAdapter](-pokemon-adapter/index.md) | [android]<br>public final class [PokemonAdapter](-pokemon-adapter/index.md) extends [RecyclerView.Adapter](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.Adapter.html)&lt;[PokemonAdapter.ViewHolder](-pokemon-adapter/-view-holder/index.md)&gt; |
| [PokemonDetailViewModel](-pokemon-detail-view-model/index.md) | [common]<br>public final class [PokemonDetailViewModel](-pokemon-detail-view-model/index.md) extends BaseViewModel implements KoinComponent |
| [PokemonStatAdapter](-pokemon-stat-adapter/index.md) | [android]<br>public final class [PokemonStatAdapter](-pokemon-stat-adapter/index.md) extends [RecyclerView.Adapter](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.Adapter.html)&lt;[PokemonStatAdapter.ViewHolder](-pokemon-stat-adapter/-view-holder/index.md)&gt; |
