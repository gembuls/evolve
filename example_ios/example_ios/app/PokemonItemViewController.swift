import UIKit
import evolve_shared

class PokemonItemViewController: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    static let identifier = "PokemonItemView"
    static func nib() -> UINib {
        return UINib(nibName: "PokemonItemView", bundle: nil)
    }

    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configure(with item: PokemonUrl) {
        if let p = item.pokemon {
            if let it = p.image {
                imageView?.backgroundColor = .clear
                imageView?.downloaded(from: it)
                imageView?.contentMode = .scaleAspectFill
            }
        } else {
            imageView?.image = nil
            imageView?.backgroundColor = .grey20
        }
    }

}
