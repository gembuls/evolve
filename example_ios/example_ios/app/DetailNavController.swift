import UIKit
import core
import core_shared

class DetailNavController: UIViewController {
    @IBOutlet weak var infoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let evolving = Preferences.value(forKey: AppConstant.shared.EVOLVING_KEY, defaultValue: "")
        infoLabel.text = "You have chosen \(evolving)!"
    }
    
}
