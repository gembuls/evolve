import UIKit
import evolve
import evolve_shared

class PokemonDetailView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nextTitle: UILabel!
    @IBOutlet weak var nextNameLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var progressInfoLabel: UILabel!
    @IBOutlet weak var primaryButton: UIButton!
    @IBOutlet weak var secondaryButton: UIButton!
    
    @IBAction func primaryAction(_ sender: Any) {
        if let action = onPrimaryAction {
            action()
        }
    }
    
    @IBAction func secondaryAction(_ sender: Any) {
        if let action = onSecondaryAction {
            action()
        }
    }
    
    private var currentWeight = 0
    private var onPrimaryAction: (() -> Void)? = nil
    private var onSecondaryAction: (() -> Void)? = nil
    
    static let identifier = "PokemonDetailView"
    static func nib() -> UINib {
        return UINib(nibName: "PokemonDetailView", bundle: nil)
    }
    
    override init (frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initSubviews()
    }

    func initSubviews() {
        PokemonDetailView.nib().instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        progressView.tintColor = .primary
        primaryButton.tintColor = .primary
        secondaryButton.tintColor = .primary
        secondaryButton.layer.borderColor = UIColor.primary.cgColor
        secondaryButton.layer.borderWidth = 1
        secondaryButton.layer.cornerRadius = secondaryButton.bounds.height / 2
        addSubview(contentView)
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(item: Pokemon, onPrimaryAction: (() -> Void)?, onSecondaryAction: @escaping () -> Void) {
        nextTitle.isHidden = true
        nextNameLabel.isHidden = true
        progressView.isHidden = true
        progressView.setProgress(0, animated: false)
        progressInfoLabel.isHidden = true
        primaryButton.isHidden = true
        
        if let i = item.image {
            imageView.downloaded(from: i)
        }
        if let n = item.name {
            nameLabel.text = n.capitalized
        }
        if let w = item.weight {
            currentWeight = Int(truncating: w)
        }
        if let a = onPrimaryAction {
            self.primaryButton.isHidden = false
            self.onPrimaryAction = a
        }
        self.onSecondaryAction = onSecondaryAction
    }
    
    func evolution(item: Pokemon) {
        nextTitle.isHidden = false
        nextNameLabel.isHidden = false
        progressView.isHidden = false
        progressInfoLabel.isHidden = false
        if let n = item.name {
            nextNameLabel.text = n.capitalized
        }
        if let w = item.weight {
            progressInfoLabel.text = "(\(currentWeight) / \(w)) Kg"
            let progress = Float(currentWeight) / Float(truncating: w)
            progressView.setProgress(progress * 100, animated: false)
        }
    }
}
