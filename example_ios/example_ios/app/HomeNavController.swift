import UIKit
import core
import core_shared
import evolve
import evolve_shared

class HomeNavController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pokemonAdapter: PokemonAdapter!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var detailContainerView: PokemonDetailView!
    
    let inset: CGFloat = 10
    let minimumLineSpacing: CGFloat = 10
    let minimumInteritemSpacing: CGFloat = 10
    let cellsPerRow = 3
    private var refreshControl: UIRefreshControl!
    private var vm = PokemonListViewModel()
    private var vmDetail = PokemonDetailViewModel()
    private var selected = ""
    private var nextPokemonName: String? = nil
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        scrollView.refreshControl = refreshControl
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        subTitleLabel.textColor = .title
        detailContainerView.superview?.bringSubviewToFront(detailContainerView)
    }
    
    @objc func onRefresh() {
        refreshControl.endRefreshing()
        pokemonAdapter.clear()
        vm.page = 0
        vm.getPokemonsLocal(limit: 100000, searchArg: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.sizeToFit()
        
        setupPokemonsAdapter()
        setupObservable()
        vm.getPokemonsLocal(limit: 100000, searchArg: nil)
    }
}

// MARK: - ScrollViewDelegate
extension HomeNavController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height) {
            scrollView.isScrollEnabled = false
            pokemonAdapter.isScrollEnabled = true
        }
    }
}

// MARK: - Action
extension HomeNavController {
    func setupPokemonsAdapter() {
        let marginsAndInsets = inset * 2 + pokemonAdapter.safeAreaInsets.left + pokemonAdapter.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((pokemonAdapter.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        pokemonAdapter.itemSize = CGSize(width: itemWidth, height: itemWidth)
        pokemonAdapter.shadowOpacity = 0.1
        pokemonAdapter.fetchData = {
            self.loadMore()
        }
        pokemonAdapter.isScrollEnabled = false
        pokemonAdapter.onScrolled = { scrollview in
            if scrollview.contentOffset.y <= 0 && self.pokemonAdapter.isScrollEnabled {
                self.scrollView.isScrollEnabled = true
                self.pokemonAdapter.isScrollEnabled = false
            }
        }
        pokemonAdapter.onSelected = { item in
            self.selected = item.name!
            self.vmDetail.name = item.name!
            if item.pokemon == nil {
                self.vmDetail.getPokemonDetailLocal(nameArg: item.name!)
            } else {
                self.vmDetail.pokemon.setValue(item.pokemon)
            }
        }
        let skl = PokemonUrl()
        skl.name = ""
        pokemonAdapter.skeleton = skl
    }
    
    func setupObservable() {
        vm.loadingIndicator.collect(collector: Collector<Bool> { isLoading in
            self.onLoading(isLoading)
        }){ e in }
        vm.successMessage.collect(collector: Collector<String> { pokemons in
            //show success message
        }){ e in }
        vm.errorMessage.collect(collector: Collector<String> { pokemons in
            //show error message
        }){ e in }
        vm.allPokemons.collect(collector: Collector<[PokemonUrl]> { pokemons in
            //autocomplete
        }){ e in }
        vm.pokemons.collect(collector: Collector<[PokemonUrl]> { pokemons in
            if let its = pokemons {
                self.pokemonAdapter.addItems(its.count) {
                    for it in its {
                        self.pokemonAdapter.items.append(it)
                        self.pokemonAdapter.itemsCache.append(it)
                    }
                }
                for it in its {
                    if it.pokemon == nil {
                        self.vmDetail.getPokemonDetailLocal(nameArg: it.name!)
                    }
                }
                self.vm.pokemons.setValue(nil)
            }
        }){ e in }
        vm.pokemon.collect(collector: Collector<Pokemon> { pokemon in
            //implement filter on chosen pokemon
            self.vm.pokemon.setValue(nil)
        }){ e in }
        
        vmDetail.pokemon.collect(collector: Collector<Pokemon> { pokemon in
            if let p = pokemon {
                if let n = p.name {
                    if self.vmDetail.name.isEmpty {
                        if let exist = self.pokemonAdapter.items.first(where: { $0.name == n }) {
                            let temp = exist.clone()
                            temp.pokemon = p
                            if let i = self.pokemonAdapter.items.firstIndex(of: exist) {
                                self.pokemonAdapter.items[i] = temp
                                self.pokemonAdapter.itemsCache[i] = temp
                                UIView.performWithoutAnimation {
                                    self.pokemonAdapter.reloadItems(at: [IndexPath(row: i, section: 0)])
                                }
                            }
                        }
                    } else if self.selected == n {
                        self.detailContainerView.configure(
                            item: p,
                            onPrimaryAction: {
                                self.vmDetail.add(nameArg: n)
                            },
                            onSecondaryAction: {
                                self.detailContainerView.isHidden = true
                            }
                        )
                        self.detailContainerView.isHidden = false
                        self.vmDetail.id = Int32(truncating: p.id!)
                        if let e = p.evolution {
                            let test = self.vmDetail.checkEvolution(pokemonEvolution: e, name: n)
                            self.nextPokemonName = test
                        }
                    }
                }
                self.vmDetail.pokemon.setValue(nil)
            }
        }){ e in }
        vmDetail.nextPokemon.collect(collector: Collector<Pokemon> { pokemon in
            if let p = pokemon, p.name == self.nextPokemonName {
                self.detailContainerView.evolution(item: p)
                self.vmDetail.nextPokemon.setValue(nil)
            }
        }){ e in }
        vmDetail.onUpdateParent.collect(collector: Collector<Pokemon> { pokemon in
            //onchosen pokemon
            self.vmDetail.onUpdateParent.setValue(nil)
        }){ e in }
    }

    func loadMore() {
        vm.page += 1
        vm.getPokemonsLocal(limit: AppConstant.shared.LIST_LIMIT, searchArg: nil)
    }

    func onLoading(_ isLoading: Bool?) {
        if let showLoading = isLoading {
            if pokemonAdapter.itemsCache.count == 0 && showLoading {
                pokemonAdapter.showSkeletons()
            }
        }
    }
}
