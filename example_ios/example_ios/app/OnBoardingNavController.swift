import UIKit
import core
import core_shared
import evolve_shared

class OnBoardingNavController: UIViewController {
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    
    private var vm = OnBoardingViewModel()
    
    @IBAction func nextAction(_ sender: Any) {
        vm.next()
    }
    @IBAction func SkipAction(_ sender: Any) {
        showChooseType()
    }
    
    let initialPage = 0
    var pages = [UIViewController]()
    var onBoardingAdapter: GenericPageAdapter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vm.loadingIndicator.collect(collector: Collector<Bool> { isLoading in
            if let l = isLoading { self.view.showFulLoading(l) }
        }){ e in }
        vm.onNext.collect(collector: Collector<Bool> { isNext in
            if let n = isNext, n == true {
                if self.pageControl.currentPage < self.pages.count - 1 {
                    self.pageControl.currentPage += 1
                    self.onBoardingAdapter?.goToNextPage()
                } else {
                    self.showChooseType()
                }
                self.vm.onNext.setValue(false)
            }
        }){ e in }
        vm.hasChosen.collect(collector: Collector<Bool> { isChosen in
            if let c = isChosen, c == true {
                let isMultiple = Preferences.value(forKey: AppConstant.shared.MULTIPLE_KEY, defaultValue: false)
                self.performSegue(withIdentifier: isMultiple ? "OnBoardingToBottomNav" : "OnBoardingToHomeNav", sender: nil)
                self.vm.hasChosen.setValue(false)
            }
        }){ e in }
        
        setStyle()
        onBoardingAdapter = self.children[0] as? GenericPageAdapter
        
        pageControl.addTarget(self, action: #selector(pageControlTapped(_:)), for: .valueChanged)
        pageControl.currentPage = initialPage
        onBoardingAdapter?.initialPage = initialPage
        onBoardingAdapter?.pageControl = pageControl
        
        pages.append(OnBoardingItemViewController(
            imageUrl: "explore_world",
            title: "Explore Pokemon World",
            description: "Imagine you are Ash Ketchum, a boy from the village of Pallet who wants to become a Pokemon trainer. To become a pokemon trainer, you have to choose a pokemon to go on an adventure with."
        ))
        pages.append(OnBoardingItemViewController(
            imageUrl: "start_evolve",
            title: "Start Evolve Your Pokemon",
            description: "Each pokemon has its own base stat. One of them is weight (Kg). You have to feed your pokemon with a berry every day. For each given berry, your Pokemon\'s weight will increase according to the firmness of the berry."
        ))
        onBoardingAdapter?.pages = pages
    }
    
    @objc func pageControlTapped(_ sender: UIPageControl) {
        onBoardingAdapter?.setViewControllers([pages[sender.currentPage]], direction: .forward, animated: true, completion: nil)
    }
    
    func setStyle() {
        skipBtn.layer.borderWidth = 1
        skipBtn.layer.cornerRadius = skipBtn.bounds.height / 2
        skipBtn.layer.borderColor = UIColor.primary.cgColor
        skipBtn.tintColor = .primary
        skipBtn.titleLabel?.font = UIFont(name: "poppins_semi_bold", size: CGFloat(13))
        
        nextBtn.tintColor = .primary
        nextBtn.titleLabel?.font = UIFont(name: "poppins_semi_bold", size: CGFloat(13))
    }
    
    func showChooseType() {
        let chooseSheet = UIAlertController(title: "Type", message: "Choose Title?", preferredStyle: UIAlertController.Style.actionSheet)
        chooseSheet.addAction(UIAlertAction(title: "Single", style: UIAlertAction.Style.default) { action in
            Preferences.value(forKey: AppConstant.shared.EVOLVING_KEY, value: "")
            Preferences.value(forKey: AppConstant.shared.ONBOARDING_KEY, value: true)
            Preferences.value(forKey: AppConstant.shared.MULTIPLE_KEY, value: false)
            self.vm.choose()
        })
        chooseSheet.addAction(UIAlertAction(title: "Multiple", style: UIAlertAction.Style.default) { action in
            Preferences.value(forKey: AppConstant.shared.EVOLVING_KEY, value: "")
            Preferences.value(forKey: AppConstant.shared.ONBOARDING_KEY, value: true)
            Preferences.value(forKey: AppConstant.shared.MULTIPLE_KEY, value: true)
            self.vm.choose()
        })
        chooseSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(chooseSheet, animated: true, completion: nil)
    }
}
