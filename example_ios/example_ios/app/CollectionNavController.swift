import UIKit
import core
import core_shared
import evolve_shared

class CollectionNavController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pokemonAdapter: PokemonAdapter!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var emptyInfoView: UILabel!
    
    let inset: CGFloat = 10
    let minimumLineSpacing: CGFloat = 10
    let minimumInteritemSpacing: CGFloat = 10
    let cellsPerRow = 2
    private var refreshControl: UIRefreshControl!
    private var vm = PokemonListViewModel()
    private var vmDetail = PokemonDetailViewModel()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh), for: .valueChanged)
        scrollView.refreshControl = refreshControl
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
    }
    
    @objc func onRefresh() {
        refreshControl.endRefreshing()
        pokemonAdapter.clear()
        vm.page = 0
        vm.getPokemonsLocal(limit: 100000, searchArg: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.sizeToFit()
        
        setupPokemonsAdapter()
        setupObservable()
        vm.isChosen = true
        vm.getPokemonsLocal(limit: 100000, searchArg: nil)
    }
}

// MARK: - ScrollViewDelegate
extension CollectionNavController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height) {
            scrollView.isScrollEnabled = false
            pokemonAdapter.isScrollEnabled = true
        }
    }
}

// MARK: - Action
extension CollectionNavController {
    func setupPokemonsAdapter() {
        let marginsAndInsets = inset * 2 + pokemonAdapter.safeAreaInsets.left + pokemonAdapter.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((pokemonAdapter.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        pokemonAdapter.itemSize = CGSize(width: itemWidth, height: itemWidth)
        pokemonAdapter.shadowOpacity = 0.1
        pokemonAdapter.fetchData = {
            self.loadMore()
        }
        pokemonAdapter.isScrollEnabled = false
        pokemonAdapter.onScrolled = { scrollview in
            if scrollview.contentOffset.y <= 0 && self.pokemonAdapter.isScrollEnabled {
                self.scrollView.isScrollEnabled = true
                self.pokemonAdapter.isScrollEnabled = false
            }
        }
        pokemonAdapter.onSelected = { item in
            Preferences.value(forKey: AppConstant.shared.EVOLVING_KEY, value: item.name!)
            self.performSegue(withIdentifier: "HomeToDetailNav", sender: nil)
        }
        let skl = PokemonUrl()
        skl.name = ""
        pokemonAdapter.skeleton = skl
    }
    
    func setupObservable() {
        vm.loadingIndicator.collect(collector: Collector<Bool> { isLoading in
            self.onLoading(isLoading)
        }){ e in }
        vm.successMessage.collect(collector: Collector<String> { pokemons in
            //show success message
        }){ e in }
        vm.errorMessage.collect(collector: Collector<String> { pokemons in
            //show error message
        }){ e in }
        vm.allPokemons.collect(collector: Collector<[PokemonUrl]> { pokemons in
            if let its = pokemons {
                var temps = [String]()
                for it in its {
                    if let n = it.name, !n.isEmpty {
                        temps.append(n)
                    }
                }
                if !temps.isEmpty {
                    //autocomplete
                    self.emptyInfoView.isHidden = true
                } else {
                    self.emptyInfoView.isHidden = false
                }
            }
        }){ e in }
        vm.pokemons.collect(collector: Collector<[PokemonUrl]> { pokemons in
            if let its = pokemons {
                self.pokemonAdapter.addItems(its.count) {
                    for it in its {
                        self.pokemonAdapter.items.append(it)
                        self.pokemonAdapter.itemsCache.append(it)
                    }
                }
                for it in its {
                    if it.pokemon == nil {
                        self.vmDetail.getPokemonDetailLocal(nameArg: it.name!)
                    }
                }
            }
        }){ e in }
        vm.pokemon.collect(collector: Collector<Pokemon> { pokemon in
            //implement filter on choson pokemon
        }){ e in }
        
        vmDetail.pokemon.collect(collector: Collector<Pokemon> { pokemon in
            if let it = pokemon {
                if let n = it.name {
                    if self.vmDetail.name.isEmpty {
                        if let exist = self.pokemonAdapter.items.first(where: { $0.name == n }) {
                            let temp = exist.clone()
                            temp.pokemon = it
                            if let i = self.pokemonAdapter.items.firstIndex(of: exist) {
                                self.pokemonAdapter.items[i] = temp
                                self.pokemonAdapter.itemsCache[i] = temp
                                UIView.performWithoutAnimation {
                                    self.pokemonAdapter.reloadItems(at: [IndexPath(row: i, section: 0)])
                                }
                            }
                        }
                    } else {
                        //showDetail
                    }
                }
            }
        }){ e in }
        vmDetail.nextPokemon.collect(collector: Collector<Pokemon> { pokemon in
            //update next pokemon
        }){ e in }
        vmDetail.onUpdateParent.collect(collector: Collector<Pokemon> { pokemon in
            //onchosen pokemon
        }){ e in }
    }

    func loadMore() {
        vm.page += 1
        vm.getPokemonsLocal(limit: AppConstant.shared.LIST_LIMIT, searchArg: nil)
    }

    func onLoading(_ isLoading: Bool?) {
        if let showLoading = isLoading {
            if pokemonAdapter.itemsCache.count == 0 && showLoading {
                pokemonAdapter.showSkeletons()
            }
        }
    }
}
