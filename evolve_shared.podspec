Pod::Spec.new do |spec|
    spec.name                     = 'evolve_shared'
    spec.version                  = '0.1.0'
    spec.homepage                 = 'https://gitlab.com/kmm-pokemon/evolve'
    spec.source                   = { :http=> ''}
    spec.authors                  = ''
    spec.license                  = ''
    spec.summary                  = 'Provide browse and evolve pokemon'
    spec.vendored_frameworks      = 'evolve_shared/build/XCFrameworks/release/core_shared.xcframework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '14.1'
end