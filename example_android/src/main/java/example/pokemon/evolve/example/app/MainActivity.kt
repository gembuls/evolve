package example.pokemon.evolve.example.app

import android.content.SharedPreferences
import example.pokemon.core.shared.app.common.BaseActivity
import example.pokemon.core.shared.external.constant.AppConstant
import example.pokemon.evolve.example.R
import org.koin.core.component.inject
import example.pokemon.evolve.shared.R as eR

class MainActivity : BaseActivity() {
    private val sharedPreferences: SharedPreferences by inject()

    override fun appVersion() = getString(R.string.app_version)
    override fun navGraph(): Int {
        val onBoarding = sharedPreferences.getBoolean(AppConstant.ONBOARDING_KEY, false)
        return if (onBoarding) {
            val evolving = sharedPreferences.getString(AppConstant.EVOLVING_KEY, "")
            if (evolving.isNullOrEmpty()) R.navigation.main_nav_graph
            else eR.navigation.evolve_nav_graph
        } else eR.navigation.onboarding_nav_graph
    }

    override fun topLevelDestinations(): Set<Int> {
        val list = HashSet<Int>()
        list.add(eR.id.exploreFragment)
        list.add(eR.id.collectionsFragment)
        return list
    }
}