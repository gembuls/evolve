# Traveler Core

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![pipeline status](https://gitlab.com/kmm-pokemon/evolve/badges/main/pipeline.svg)](https://gitlab.com/kmm-pokemon/evolve/-/commits/main) [![coverage report](https://gitlab.com/kmm-pokemon/evolve/badges/main/coverage.svg)](https://gitlab.com/kmm-pokemon/evolve/-/commits/main) [![Latest Release](https://gitlab.com/kmm-pokemon/evolve/-/badges/release.svg)](https://gitlab.com/kmm-pokemon/evolve/-/releases)

## Features

- Provide onboarding story with 2 mode
    - Single to evolve 1 pokemon
    - Multiple to evolve multiple pokemon
- Provide explore pokemon screen with search autocomplete name
- Provide see detail pokemon with animation
- Provide list saved collections pokemon
- Provide detail of pokemon:
    - Drag and drop berry to feed pokemon
    - Animated progress to evolve
    - Shining animation after progress fully achieved
    - Evolve pokemon
    - Delete pokemon with dialog confirmation
- Powered by KMM with KOIN for dependency injection and using MVVM pattern with clean architecture.

## Contents

- [Features](#features)
- [Requirements](#requirements)
- [Architectural Pattern](#architectural-pattern)
- [Project Structure](#project-structure)
- [Getting started](#getting-started)
- [Example](https://gitlab.com/kmm-pokemon/evolve/tree/main/example_android)
- [Documentation](https://gitlab.com/kmm-pokemon/evolve/tree/main/docs)
- [Development](#development)

## Requirements

1. Minimum Android/SDK Version: Nougat/23
2. Deployment Target iOS/SDK Version: 14.1
3. Target Android/SDK Version: Snow Cone/32
4. Compile Android/SDK Version: Snow Cone/32
5. This project is built using Android Studio version 2022.1.1 and Android Gradle 7.5

## Architectural Pattern

This project implement
Clean [Architecture by Fernando Cejas](https://github.com/android10/Android-CleanArchitecture)

### Clean architecture

![Image Clean architecture](/resources/clean_architecture.png)

### Architectural approach

![Image Architectural approach](/resources/clean_architecture_layers.png)

### Architectural reactive approach

![Image Architectural reactive approach](/resources/clean_architecture_layers_details.png)

## Project Structure

For the high level hierarchy, the project separate into 3 main modules, which are :

### App

> This module represent the Presentation layer. Consists of activities, fragments, views, etc. The
> classes are separated based on features, for examples : auth, booking, easyride, etc.

### Domain

> This module represent the Domain layer. Consists of interactors/use cases and models and doesn’t
> know anything outside.

### Data

> This module represent the Data layer. Consists of data sources; can be network call, mock data,
> disk data, and cache data.

## Getting started

1. Fork this repository to your account
2. Copy forked repository ID, paste for step 4
3. Create new `Private Token`
   -> [Tutorial](https://docs.gitlab.com/ee/user/project/private_tokens/index.html)
4. Check for `read_package_registry` role, then save your token
5. Create `properties.gradle` in your root folder, add this content:

```groovy
ext {
    gitlab = [
            consumeToken: "<Generated Private Token>"
    ]
}
```

4. Edit settings.gradle in your root folder:

```groovy
dependencyResolutionManagement {
    repositories {
        //...
        maven {
            name = "Evolve"
            url = uri("https://gitlab.com/api/v4/projects/<FORKED_REPOSITORY_ID>/packages/maven")
            credentials(HttpHeaderCredentials) {
                name = 'Private-Token'
                value = gitlab.consumeToken
            }
            authentication {
                header(HttpHeaderAuthentication)
            }
        }
    }
}
```

5. Last, add 'implementation "example.pokemon.evolve:${buildType}:${version}"' inside tag
   dependencies { . . . } of build.gradle app

## Development

Here are some useful gradle/adb commands for executing this example:

* ./gradlew clean build - Build the entire project and execute unit tests
* ./gradlew clean sonarqube - Execute sonarqube coverage report
* ./gradlew dokkaGfm - Generate documentation
* ./gradlew test[flavor][buildType]UnitTest - Execute unit tests e.g., testDevDebugUnitTest
* ./gradlew test[flavor][buildType]UnitTest create[flavor][buildType]CoverageReport - Execute unit
  tests and create coverage report e.g., createDevDebugCoverageReport
* ./gradlew assemble[flavor][buildType] - Create apk file e.g., assembleDevDebug
* ./gradlew :evolve_shared:assembleXCFramework - Generate XCFramework for iOS